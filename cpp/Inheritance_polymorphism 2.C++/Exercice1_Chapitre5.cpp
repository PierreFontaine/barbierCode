#include <cassert>

#include "Exercice1_Chapitre5.h"

const float PEL::Taux_interet = 2.5F;

PEL::PEL(int id, float solde) : Compte_epargne(id, solde) {
}

float PEL::taux_interet() const {
    return Taux_interet;
}

const float PEA::Plafond_de_depot = 132000.F;

PEA::PEA(int id, float solde) : Compte_bancaire(id, solde) {
}

float PEA::taux_interet() const {
    throw "Le taux d'intérêt n'a pas de sens";
} // Cette solution montre que la présence de la fonction abstraite 'taux_interet' dans 'Compte_bancaire' n'est pas forcément un bon choix

void PEA::appliquer_taux_interet() {
    /* Cela dépend de l'évolution du cours des actions du portefeuille de ce compte... */
};

