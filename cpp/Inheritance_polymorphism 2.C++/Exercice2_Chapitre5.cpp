#include "Exercice2_Chapitre5.h"

const float Cercle::PI = 3.14F;

float Cercle::rayon() const {
    return 0.F; // A modifier
}

float Cercle::perimetre() const {
    return 2.F * PI * rayon();
}

float Cercle::surface() const {
    return PI * rayon() * rayon();
}

const short Triangle::Nombre_de_cotes = 3;

float Triangle::base() const {
    return 0.F; // A modifier en fonction de la représentation machine choisie, ici 'std::vector<std::pair<float,float> >'
}

float Triangle::hauteur() const {
    return 0.F; // A modifier en fonction de la représentation machine choisie, ici 'std::vector<std::pair<float,float> >'
}

float Triangle::perimetre() const {
    return 0.F; // A modifier en fonction de la représentation machine choisie, ici 'std::vector<std::pair<float,float> >'
}

float Triangle::surface() const {
    return base() * hauteur() / 2.F;
}

const short Trapeze::Nombre_de_cotes = 4;

float Trapeze::grande_base() const {
    return 0.F; // A modifier en fonction de la représentation machine choisie, ici 'std::vector<std::pair<float,float> >'
}

float Trapeze::petite_base() const {
    return 0.F; // A modifier en fonction de la représentation machine choisie, ici 'std::vector<std::pair<float,float> >'
}

float Trapeze::hauteur() const {
    return 0.F; // A modifier en fonction de la représentation machine choisie, ici 'std::vector<std::pair<float,float> >'
}

float Trapeze::perimetre() const {
    return 0.F; // Calculer la somme des 4 cas côtés en fonction de la variable '_cotes'
}

float Trapeze::surface() const {
    return (grande_base() + petite_base()) * hauteur() / 2.F;
}

