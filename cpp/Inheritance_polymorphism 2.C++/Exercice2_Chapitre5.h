#ifndef _Exercice2_Chapitre5
#define _Exercice2_Chapitre5

#include <utility> // 'std::pair'
#include <vector>

class Forme {
public:
    virtual float perimetre() const = 0;
    virtual float surface() const = 0;
};

class Cercle : public Forme {
public:
    const static float PI;
    float rayon() const;
    virtual float perimetre() const;
    virtual float surface() const;
};

class Triangle : public Forme { // On ne rend pas la classe 'Triangle' abstraite pour pouvoir créer des triangles qui ne sont ni rectangle, ni isocèle, ni équilatéral !
    std::vector<std::pair<float, float> > _cotes;
public:
    const static short Nombre_de_cotes;
    float base() const;
    float hauteur() const;
    virtual float perimetre() const;
    virtual float surface() const;
};
// Pour tous les sous-types de triangle, la question se pose de surcharger les fonctions :
// 'float perimetre() const;' et 'float surface() const;'
// A priori, cela n'est pas nécessaire car le calcul proposé dans la classe 'Triangle' lui-même fonctionne
// Ceci étant, cela pourrait se justifier pour fournir un calcul *plus rapide* et donc court-circuité le calcul standard implanté dans 'Triangle'	

class Triangle_rectangle : public Triangle {
};

class Triangle_isocele : public Triangle {
};

class Triangle_equilateral : public Triangle_isocele {
};

// Un trapèze est un quadrilatère convexe, on ne s'intéresse pas aux quadrilatères non convexes
// Sinon, pour tous les sous-types de trapèze, la question de la surcharge se pose comme pour les triangles

class Trapeze : public Forme {
    std::vector<std::pair<float, float> > _cotes;
public:
    const static short Nombre_de_cotes;
    float grande_base() const;
    float petite_base() const;
    float hauteur() const;
    virtual float perimetre() const;
    virtual float surface() const;
};

class Parallelogramme : public Trapeze {
};

class Rectangle : public Parallelogramme {
};

class Carre : public Rectangle {
};

#endif
