#ifndef _Exercice1_Chapitre5
#define _Exercice1_Chapitre5

#include "Polymorphisme.h"

class PEL : public Compte_epargne {
public:
    const static float Taux_interet;
    PEL(int, float = 0.F);
    virtual float taux_interet() const;
};

typedef Livret_A Ldd; // Les deux livrets sont confondus

class PEA : public Compte_bancaire { // Le PEA n'est ni véritablement un compte épargne, ni un compte chèque
public:
    const static float Plafond_de_depot;
    PEA(int, float = 0.F);
    virtual float taux_interet() const;
    virtual void appliquer_taux_interet(); // A surcharger car version inappropriée dans 'Compte_bancaire'
};

#endif
