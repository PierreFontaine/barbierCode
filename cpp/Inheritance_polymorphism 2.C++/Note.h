#ifndef _Note
#define _Note

#include <sstream>
#include <string>

class Note {
private:
    static int _Increment;
    std::istringstream * const _source; // '#include <sstream>'
public:
    const static std::string Default_header; // '#include <string>'
protected:
    std::string _text;
private:
    virtual void initialization();
    void collect_data();
public:
    Note(std::istringstream * const);
    virtual ~Note();
    virtual std::string write();
};

class Confidential_note : public Note {
public:
    const static std::string Default_header;
private:
    void initialization();
public:
    Confidential_note(std::istringstream * const);
    virtual std::string write();
};

class Top_secret_note : public Confidential_note {
public:
    const static std::string Default_header;
private:
    void initialization();
public:
    Top_secret_note(std::istringstream * const);
    virtual std::string write();
private:
    virtual std::string encrypt(std::string&);
};

#endif
