#include <cmath>

#include <cassert>

#include "Activity_card.h"

extern "C" double fabs(double); // <math.h>

Activity_card::Activity_card(const unsigned short cn, const unsigned short y) : /*_implementation(12),*/client_number(cn), year(y), Vector_quadratic_norm<Vector_quadratic_norm<double> >(12), Vector_maximum_norm<Vector_maximum_norm<double> >(12) {
    assert(client_number > 0);
    //for(int i = Vector_quadratic_norm<Vector_quadratic_norm<double> >::Min_index;i < Vector_quadratic_norm<Vector_quadratic_norm<double> >::Min_index + 12;i++) _implementation[i] = Vector_quadratic_norm<double>(client_number);

    //for(int i = Vector_quadratic_norm<Vector_quadratic_norm<double> >::Min_index;i < Vector_quadratic_norm<Vector_quadratic_norm<double> >::Min_index + 12;i++) Vector_quadratic_norm<Vector_quadratic_norm<double> >::_implementation[i] = Vector_quadratic_norm<double>(client_number);

}

double Activity_card::turnover_indicator_qn() const {
    //return _implementation.norm();
    return Vector_quadratic_norm<Vector_quadratic_norm<double> >::norm();
}

double Activity_card::turnover_indicator_mn() const {
    /*Vector_maximum_norm<Vector_maximum_norm<double> > as_VMN(12);
    for(int i = Vector_maximum_norm<Vector_maximum_norm<double> >::Min_index;i < Vector_maximum_norm<Vector_maximum_norm<double> >::Min_index + 12;i++) {
            as_VMN[i] = Vector_maximum_norm<double>(client_number);
            for(int j = Vector_maximum_norm<double>::Min_index;j < Vector_maximum_norm<double>::Min_index + client_number;j++) as_VMN[i][j] = _implementation[i][j];
    }
    return as_VMN.norm();*/
    return 0.;
}

double Activity_card::turnover_indicator_sygma_qn(const Activity_card& activity_card) const {
    //return (_implementation + activity_card._implementation).norm();
    return 0.;
}

double Activity_card::turnover_indicator_delta_qn(const Activity_card& activity_card) const {
    //return (_implementation - activity_card._implementation).norm();
    return 0.;
}

double Activity_card::turnover_indicator_sygma_mn(const Activity_card& activity_card) const {
    /*	Vector_maximum_norm<Vector_maximum_norm<double> > this_as_VMN(12),activity_card_as_VMN(12);
            for(int i = Vector_maximum_norm<Vector_maximum_norm<double> >::Min_index;i < Vector_maximum_norm<Vector_maximum_norm<double> >::Min_index + 12;i++) {
                    this_as_VMN[i] = Vector_maximum_norm<double>(this->client_number);
                    activity_card_as_VMN[i] = Vector_maximum_norm<double>(activity_card.client_number);
                    for(int j = Vector_maximum_norm<double>::Min_index;j < Vector_maximum_norm<double>::Min_index + client_number;j++) {
                            this_as_VMN[i][j] = this->_implementation[i][j];
                            activity_card_as_VMN[i][j] = activity_card._implementation[i][j];
                    }
            }
            return (this_as_VMN + activity_card_as_VMN).norm();*/
    return 0.;
}

double Activity_card::turnover_indicator_delta_mn(const Activity_card& activity_card) const {
    /*Vector_maximum_norm<Vector_maximum_norm<double> > this_as_VMN(12),activity_card_as_VMN(12);
    for(int i = Vector_maximum_norm<Vector_maximum_norm<double> >::Min_index;i < Vector_maximum_norm<Vector_maximum_norm<double> >::Min_index + 12;i++) {
            this_as_VMN[i] = Vector_maximum_norm<double>(this->client_number);
            activity_card_as_VMN[i] = Vector_maximum_norm<double>(activity_card.client_number);
            for(int j = Vector_maximum_norm<double>::Min_index;j < Vector_maximum_norm<double>::Min_index + client_number;j++) {
                    this_as_VMN[i][j] = this->_implementation[i][j];
                    activity_card_as_VMN[i][j] = activity_card._implementation[i][j];
            }
    }
    return (this_as_VMN - activity_card_as_VMN).norm();*/
    return 0.;
}

void Activity_card::set2008() {
    assert(client_number >= 3);
    /*	_implementation[Vector_quadratic_norm<Vector_quadratic_norm<double> >::Min_index][Vector_quadratic_norm<double>::Min_index] = 3.3;
            _implementation[Vector_quadratic_norm<Vector_quadratic_norm<double> >::Min_index][Vector_quadratic_norm<double>::Min_index + 1] = 0.;
            _implementation[Vector_quadratic_norm<Vector_quadratic_norm<double> >::Min_index][Vector_quadratic_norm<double>::Min_index + 2] = 9.7;
            _implementation[Vector_quadratic_norm<Vector_quadratic_norm<double> >::Min_index + 1][Vector_quadratic_norm<double>::Min_index] = 0.;
            _implementation[Vector_quadratic_norm<Vector_quadratic_norm<double> >::Min_index + 1][Vector_quadratic_norm<double>::Min_index + 1] = 5.6;
            _implementation[Vector_quadratic_norm<Vector_quadratic_norm<double> >::Min_index + 1][Vector_quadratic_norm<double>::Min_index + 2] = 6.8;
            // other months here
            _implementation[Vector_quadratic_norm<Vector_quadratic_norm<double> >::Min_index + 11][Vector_quadratic_norm<double>::Min_index] = 6.4;
            _implementation[Vector_quadratic_norm<Vector_quadratic_norm<double> >::Min_index + 11][Vector_quadratic_norm<double>::Min_index + 1] = 8.4;
            _implementation[Vector_quadratic_norm<Vector_quadratic_norm<double> >::Min_index + 11][Vector_quadratic_norm<double>::Min_index + 2] = 2.3;

     */


    Vector_quadratic_norm<double> x(client_number);
    x[x.Min_index] = 3.3;
    x[x.Min_index + 1] = 0.;
    x[x.Min_index + 2] = 9.7;

    this->Vector<Vector_quadratic_norm<double> >::operator[](Vector<Vector_quadratic_norm<double> >::Min_index) = x;

    //std::cout << this->Vector<Vector_quadratic_norm<double> >::operator[](Vector<Vector_quadratic_norm<double> >::Min_index);

    x[x.Min_index] = 0.;
    x[x.Min_index + 1] = 5.6;
    x[x.Min_index + 2] = 6.8;
    Vector_quadratic_norm<Vector_quadratic_norm<double> >::_implementation[Vector_quadratic_norm<Vector_quadratic_norm<double> >::Min_index + 1] = x;
    x[x.Min_index] = 6.4;
    x[x.Min_index + 1] = 8.4;
    x[x.Min_index + 2] = 2.3;
    Vector_quadratic_norm<Vector_quadratic_norm<double> >::_implementation[Vector_quadratic_norm<Vector_quadratic_norm<double> >::Min_index + 2] = x;

}

/*void Activity_card::set2009() {
        assert(client_number >= 3);
        _implementation[Vector_quadratic_norm<Vector_quadratic_norm<double> >::Min_index][Vector_quadratic_norm<double>::Min_index] = 0.8;
        _implementation[Vector_quadratic_norm<Vector_quadratic_norm<double> >::Min_index][Vector_quadratic_norm<double>::Min_index + 1] = 1.4;
        _implementation[Vector_quadratic_norm<Vector_quadratic_norm<double> >::Min_index][Vector_quadratic_norm<double>::Min_index + 2] = 2.9;
        _implementation[Vector_quadratic_norm<Vector_quadratic_norm<double> >::Min_index + 1][Vector_quadratic_norm<double>::Min_index] = 0.;
        _implementation[Vector_quadratic_norm<Vector_quadratic_norm<double> >::Min_index + 1][Vector_quadratic_norm<double>::Min_index + 1] = 6.8;
        _implementation[Vector_quadratic_norm<Vector_quadratic_norm<double> >::Min_index + 1][Vector_quadratic_norm<double>::Min_index + 2] = 3.5;
        // other months here
        _implementation[Vector_quadratic_norm<Vector_quadratic_norm<double> >::Min_index + 11][Vector_quadratic_norm<double>::Min_index] = 2.9;
        _implementation[Vector_quadratic_norm<Vector_quadratic_norm<double> >::Min_index + 11][Vector_quadratic_norm<double>::Min_index + 1] = 6.9;
        _implementation[Vector_quadratic_norm<Vector_quadratic_norm<double> >::Min_index + 11][Vector_quadratic_norm<double>::Min_index + 2] = 0.;
}*/
Biennal_synthesis::Biennal_synthesis(const Activity_card& current_year, const Activity_card& previous_year, const double s, const double d) throw (std::logic_error) : _current_year(current_year), _previous_year(previous_year), lambda_for_sygma(s), lambda_for_delta(d) {
    if (_previous_year.year != _current_year.year - 1) throw std::logic_error("OCL invariant violated");
}

double Biennal_synthesis::sygma_turnover_indicator_qn() const {
    return std::fabs(lambda_for_sygma) * _current_year.turnover_indicator_sygma_qn(_previous_year);
}

double Biennal_synthesis::sygma_turnover_indicator_mn() const {
    return std::fabs(lambda_for_sygma) * _current_year.turnover_indicator_sygma_mn(_previous_year);
}

double Biennal_synthesis::delta_turnover_indicator_qn() const {
    return std::fabs(lambda_for_delta) * _current_year.turnover_indicator_delta_qn(_previous_year);
}

double Biennal_synthesis::delta_turnover_indicator_mn() const {
    return std::fabs(lambda_for_delta) * _current_year.turnover_indicator_delta_mn(_previous_year);
}
