#ifndef _Vector
#define _Vector

#ifdef _WIN32
#pragma warning (disable : 4786) // For debugging purposes with STL under Visual C++
#endif

#include <algorithm> // STL generic functions
#include <cmath>
#include <cstddef> // 'size_t'
#include <numeric> // STL accumulate generic functions
#include <map> // STL map
#include <stdexcept> // Standard exception types
#include <vector> // STL vector

namespace Franck {

    template<typename T> class Vector {
    private:
        virtual Vector<T>& create(size_t = 0) const throw (std::bad_alloc) = 0;
    protected:
        double accumulate() const;
        static std::map<const Vector<T>*, std::vector<Vector<T>*>, std::less<const Vector<T>*> > Garbage;
        std::vector<T> _implementation;
    public:
        static const int Min_index;

        explicit Vector(size_t s = 0) : _implementation(s) {
        }

        virtual ~Vector() {
            if (Garbage.find(this) != Garbage.end()) {
                typename std::vector<Vector<T>*>::iterator i;
                for (i = Garbage[this].begin(); i != Garbage[this].end(); i++) delete *i;
                Garbage.erase(this);
            }
        }

        size_t size() const {
            return _implementation.size();
        }
        virtual double norm() const = 0;

        double norm_addition(const Vector& v) const {
            return (*this +v).norm();
        }

        double norm_subtraction(const Vector& v) const {
            return (*this -v).norm();
        }

        double lambda_norm(const double lambda) const {
            return std::fabs(lambda) * norm();
        }
        T& operator[](int) throw (std::out_of_range);
        const T& operator[](int) const throw (std::out_of_range); // STL compatibility
        bool operator<(const Vector<T>&) const; // >, <= and >= are derived from < in STL
        Vector<T>& operator+(const Vector<T>&) const throw (std::logic_error);
        Vector<T>& operator+=(const Vector<T>&) throw (std::exception);
        Vector<T>& operator-(const Vector<T>&) const throw (std::logic_error);
        Vector<T>& operator-=(const Vector<T>&) throw (std::exception);
        Vector<T>& operator*(const Vector<T>&) const throw (std::logic_error);
        Vector<T>& operator*=(const Vector<T>&) throw (std::exception);
        Vector<T>& operator/(const Vector<T>&) const throw (std::logic_error);
        Vector<T>& operator/=(const Vector<T>&) throw (std::exception);
    };

    template<typename T> class Vector_quadratic_norm : virtual public Vector<T> {
    private:
        Vector<T>& create(size_t = 0) const throw (std::bad_alloc);
    public:

        explicit Vector_quadratic_norm(size_t s = 0) : Vector<T>(s) {
        }

        Vector_quadratic_norm(const Vector<T>& v) : Vector<T>(v) {
        }
        double norm() const;
        operator double();
    };

    template<typename T> class Vector_maximum_norm : virtual public Vector<T> {
    private:
        Vector<T>& create(size_t = 0) const throw (std::bad_alloc);
    public:

        explicit Vector_maximum_norm(size_t s = 0) : Vector<T>(s) {
        }

        Vector_maximum_norm(const Vector<T>& v) : Vector<T>(v) {
        }
        double norm() const;
        operator double();
    };

    /////////////////////////////////////////////////////////////////
    template<typename T> std::map<const Vector<T>*, std::vector<Vector<T>*>, std::less<const Vector<T>*> > Vector<T>::Garbage;

    template<typename T> const int Vector<T>::Min_index = 1;

    template<typename T> double Vector<T>::accumulate() const {
        return std::accumulate(_implementation.begin(), _implementation.end(), T());
    }

    template<typename T> T& Vector<T>::operator[](int i) throw (std::out_of_range) {
        if (i < Min_index || i > size()) throw std::out_of_range("");
        return _implementation[i - Min_index];
    }

    template<typename T> const T& Vector<T>::operator[](int i) const throw (std::out_of_range) {
        if (i < Min_index || i > size()) throw std::out_of_range("");
        return _implementation[i - Min_index];
    }

    template<typename T> bool Vector<T>::operator<(const Vector<T>& v) const {
        return norm() < v.norm();
    }

    template<typename T> Vector<T>& Vector<T>::operator+(const Vector<T>& v) const throw (std::logic_error) {
        if (!size() && !v.size()) return create();
        if (!size()) return create() = v;
        if (!v.size()) return create() = *this;
        if (size() != v.size()) throw std::logic_error("");
        int i = Min_index;
        Vector<T>& result = create(size());
        for (; i <= size(); i++) result[i] = operator[](i) + v[i];
        return result;
    }

    template<typename T> Vector<T>& Vector<T>::operator+=(const Vector<T>& v) throw (std::exception) {
        if (!size() && !v.size()) return *this;
        if (!size()) return *this = v;
        if (!v.size()) return *this;
        if (size() != v.size()) throw std::logic_error("");
        int i = Min_index;
        for (; i <= size(); i++) operator[](i) += v[i];
        return *this;
    }

    template<typename T> Vector<T>& Vector<T>::operator-(const Vector<T>& v) const throw (std::logic_error) {
        if (!size() && !v.size()) return create();
        if (!size()) {
            int i = Min_index;
            Vector<T>& result = create(v.size());
            for (; i <= v.size(); i++) result[i] -= v[i];
            return result;
        }
        if (!v.size()) return create() = *this;
        if (size() != v.size()) throw std::logic_error("");
        int i = Min_index;
        Vector<T>& result = create(size());
        for (; i <= size(); i++) result[i] = operator[](i) - v[i];
        return result;
    }

    template<typename T> Vector<T>& Vector<T>::operator-=(const Vector<T>& v) throw (std::exception) {
        if (!size() && !v.size()) return *this;
        if (!size()) return *this = *this -v;
        if (!v.size()) return *this;
        if (size() != v.size()) throw std::logic_error("");
        int i = Min_index;
        for (; i <= size(); i++) operator[](i) -= v[i];
        return *this;
    }

    template<typename T> Vector<T>& Vector<T>::operator*(const Vector<T>& v) const throw (std::logic_error) {
        if (!size() && !v.size()) return create();
        if (!size()) return create() = *this;
        if (!v.size()) return create() = v;
        if (size() != v.size()) throw std::logic_error("");
        int i = Min_index;
        Vector<T>& result = create(size());
        for (; i <= size(); i++) result[i] = operator[](i) * v[i];
        return result;
    }

    template<typename T> Vector<T>& Vector<T>::operator*=(const Vector<T>& v) throw (std::exception) {
        if (!size() && !v.size()) return *this;
        if (!size()) return *this;
        if (!v.size()) return *this = v;
        if (size() != v.size()) throw std::logic_error("");
        int i = Min_index;
        for (; i <= size(); i++) operator[](i) *= v[i];
        return *this;
    }

    template<typename T> Vector<T>& Vector<T>::operator/(const Vector<T>& v) const throw (std::logic_error) {
        if (!size() && !v.size()) throw std::logic_error("");
        if (!size()) return create() = *this;
        if (!v.size()) throw std::logic_error("");
        if (size() != v.size()) throw std::logic_error("");
        int i = Min_index;
        Vector<T>& result = create(size());
        for (; i <= size(); i++) result[i] = operator[](i) / v[i];
        return result;
    }

    template<typename T> Vector<T>& Vector<T>::operator/=(const Vector<T>& v) throw (std::exception) {
        if (!size() && !v.size()) throw std::logic_error("");
        if (!size()) return *this;
        if (!v.size()) throw std::logic_error("");
        if (size() != v.size()) throw std::logic_error("");
        int i = Min_index;
        for (; i <= size(); i++) operator[](i) /= v[i];
        return *this;
    }

    /////////////////////////////////////////////////////////////////

    template<typename T> Vector<T>& Vector_quadratic_norm<T>::create(size_t s) const throw (std::bad_alloc) {
        Vector<T>* result = new Vector_quadratic_norm<T>(s);
        Vector<T>::Garbage[this].push_back(result);
        return *result;
    }

    template<typename T> double Vector_quadratic_norm<T>::norm() const {
        Vector_quadratic_norm<T> multiplication = *this * * this;
        return std::sqrt(multiplication.accumulate());
    }

    template<typename T> Vector_quadratic_norm<T>::operator double() {
        return Vector<T>::accumulate();
    }
    ///////////////////////////////////////////////////////////////

    template<typename T> Vector<T>& Vector_maximum_norm<T>::create(size_t s) const throw (std::bad_alloc) {
        Vector<T>* result = new Vector_maximum_norm<T>(s);
        Vector<T>::Garbage[this].push_back(result);
        return *result;
    }

    template<typename T> double Vector_maximum_norm<T>::norm() const {
        T max = *std::max_element(Vector<T>::_implementation.begin(), Vector<T>::_implementation.end());
        T min = *std::min_element(Vector<T>::_implementation.begin(), Vector<T>::_implementation.end());
        return std::fabs(min) < std::fabs(max) ? std::fabs(max) : std::fabs(min);
    }

    template<typename T> Vector_maximum_norm<T>::operator double() {
        return norm();
    }
}

namespace Barbier = Franck;

#endif
