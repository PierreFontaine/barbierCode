#ifndef _Activity_card
#define _Activity_card

#ifdef _WIN32
#pragma warning (disable : 4786) // for debugging purposes with STL under Visual C++
#endif

#include <stdexcept>

#include "Vector.h"

using namespace Barbier;

class Activity_card : public Vector_quadratic_norm<Vector_quadratic_norm<double> >, public Vector_maximum_norm<Vector_maximum_norm<double> > {
public:
    const unsigned short client_number;
    const unsigned short year;
private:
    //Vector_quadratic_norm<Vector_quadratic_norm<double> > _implementation;
public:
    Activity_card(const unsigned short, const unsigned short);
    double turnover_indicator_qn() const;
    double turnover_indicator_mn() const;
    double turnover_indicator_sygma_qn(const Activity_card&) const;
    double turnover_indicator_delta_qn(const Activity_card&) const;
    double turnover_indicator_sygma_mn(const Activity_card&) const;
    double turnover_indicator_delta_mn(const Activity_card&) const;
    // tests
    void set2008();
    //void set2009();
};

class Biennal_synthesis {
private:
    const Activity_card& _current_year;
    const Activity_card& _previous_year;
public:
    const double lambda_for_sygma;
    const double lambda_for_delta;
    Biennal_synthesis(const Activity_card&, const Activity_card&, const double = 1., const double = 1.) throw (std::logic_error);
    double sygma_turnover_indicator_qn() const;
    double sygma_turnover_indicator_mn() const;
    double delta_turnover_indicator_qn() const;
    double delta_turnover_indicator_mn() const;
};

#endif
