#include <iostream>
#include <typeinfo> // /GR with Visual C++ to enable RTTI

#include "Vector.h"
#include "Activity_card.h"

int main(int argc, char** argv) {
    using namespace Barbier;

    Vector_quadratic_norm<Vector_quadratic_norm<double> > matrix(3);

    Vector<int>* pv;
    Vector_quadratic_norm<int> vqn, *pvqn;

    pvqn = &vqn; // Normal

    pv = &vqn; // Contra-variance

    //    pvqn = static_cast<Vector_quadratic_norm<int>*> (pv); // Compilation error

    pvqn = reinterpret_cast<Vector_quadratic_norm<int>*> (pv); // Unreliable, but this works at run-time because 'pv' points to an instance of 'Vector_quadratic_norm<int>'
    const std::type_info& ti1 = typeid (pvqn); // Effective type is computed based on the 'typeid' operator
    if (ti1 != typeid (Vector_quadratic_norm<int>*)) throw std::bad_typeid();
    std::cout << ti1.name() << std::endl;

    pvqn = dynamic_cast<Vector_quadratic_norm<int>*> (pv); // Reliable
    const std::type_info& ti2 = typeid (pvqn); // Effective type is computed based on the 'typeid' operator
    if (ti2 != typeid (Vector_quadratic_norm<int>*)) throw std::bad_typeid();
    std::cout << ti2.name() << std::endl;

    //    Activity_card ac2008(3, 2008);
    //    ac2008.set2008();
    //    cout << ac2008.year << " (QN): " << ac2008.turnover_indicator_qn() << endl;
    //    cout << ac2008.year << " (MN): " << ac2008.turnover_indicator_mn() << endl;
    /*Activity_card ac2009(3,2009);
    ac2009.set2009();
    cout << ac2009.year << " (QN): " << ac2009.turnover_indicator_qn() << endl;
    cout << ac2009.year << " (MN): " << ac2009.turnover_indicator_mn() << endl;
    Biennal_synthesis bs(ac2009,ac2008,.5,.3);
    cout << "2009 + 2008 (sygma-QN): " << bs.sygma_turnover_indicator_qn() << endl;
    cout << "2009 + 2008 (sygma-MN): " << bs.sygma_turnover_indicator_mn() << endl;
    cout << "2009 - 2008 (delta-QN): " << bs.delta_turnover_indicator_qn() << endl;
    cout << "2009 - 2008 (delta-MN): " << bs.delta_turnover_indicator_mn() << endl;*/

    return 0;
}

