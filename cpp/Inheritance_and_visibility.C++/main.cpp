#include <iostream>

#include "Inheritance_and_visibility.h"

A::A() {
    _a = "_a in A 'private:' clause, ";
    _b = "_b in A 'protected:' clause, ";
    c = "c in A 'public:' clause\n";
}

std::string A::format() const {
    return _a + _b + c;
}

B::B() {
    _a = "_a in B 'private:' clause, ";
    _b = "_b in B 'protected:' clause, ";
    c = "c in B 'public:' clause\n";
}

std::string B::format() const {
    // A::_a; // Error because 'A::_a' is 'private'
    return A::_b + A::c + _a + _b + c;
}

C::C() {
    _a = "_a in C 'private:' clause, ";
    _b = "_b in C 'protected:' clause, ";
    c = "c in C 'public:' clause\n";
}

std::string C::format() const {
    // A::_a; // Error because 'A::_a' is 'private'
    // B::_a; // Error because 'B::_a' is 'private'
    // A::_b; // Error because 'class B : private A'
    // A::c; // Error because 'class B : private A'	
    return B::_b + B::c + _a + _b + c;
}

D::D() {
    _a = "_a in D 'private:' clause, ";
    _b = "_b in D 'protected:' clause, ";
    c = "c in D 'public:' clause\n";
}

std::string D::format() const {
    // A::_a; // Error because 'A::_a' is 'private'
    // B::_a; // Error because 'B::_a' is 'private'
    // C::_a; // Error because 'C::_a' is 'private'
    // A::_b; // Error because 'class B : private A'
    // A::c; // Error because 'class B : private A'
    return C::_b + C::c + B::_b + B::c + _a + _b + c;
}

int main() {
    D* d = new D;
    std::cout << d->format().c_str() << '\n';
    C* c = d;
    std::cout << c->format().c_str() << '\n';
    // B* b = d; // Error because 'class C : protected B'
    // std::cout << d->format() << '\n';
    // A* a = d; // Error because 'class B : private A'
    // std::cout << a->format() << '\n';
    return 0;
}
