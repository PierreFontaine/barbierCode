#ifndef _Inheritance_and_visibility_h
#define _Inheritance_and_visibility_h

#include <string>

class A {
    std::string _a;
protected:
    std::string _b;
public:
    std::string c;
    A();
    virtual std::string format() const;
};

class B : private A {
    std::string _a;
protected:
    std::string _b;
public:
    std::string c;
    B();
    virtual std::string format() const;
};

class C : protected B {
    std::string _a;
protected:
    std::string _b;
public:
    std::string c;
    C();
    virtual std::string format() const;
};

class D : public C {
    std::string _a;
protected:
    std::string _b;
public:
    std::string c;
    D();
    virtual std::string format() const;
};

#endif
