#include <iostream>
#include <string>
#include <vector>

class Real {
private: // "implementation" part
    std::vector<char> _implementation; // Machine-based representation through a predefined type, i.e., "std::vector" ('char' plays the role of byte)
public: // "interface" part
    Real(std::string); // To create huge Real values beyond 'double' capacity
};

Real::Real(std::string s) {

}

class Prisoner {
private:
    const std::string _file_number;
public:
    Prisoner(const std::string&);
    bool equals(const Prisoner&) const;
};

Prisoner::Prisoner(const std::string& file_number) : _file_number(file_number) {
}

bool Prisoner::equals(const Prisoner& p) const {
    if (this->_file_number.compare(p._file_number) != 0) return false;
    return true;
}

int main(int argc, char** argv) {

    Real r("99999999999999999999999999999999999999999999999999999");

    Prisoner p1("FR"), p2("FR");
    if (p1.equals(p2) == true) std::cout << "true" << std::endl;

    return 0;
}

