#include <iostream>
#include <list>
#include <queue>

#include "Using_heritance_well.h"

void Secret::cypher() {
    std::cout << "Secret: let's cypher..." << std::endl;
}

void Secret::send_message() {
    cypher();
}

void Hack::cypher() {
    std::cout << "Hack: let's bypass 'cypher'!" << std::endl;
}

int main(int argc, char** argv) {
    Bound_stack<char> bs(1);
    bs.push('a');
    bs.pop(); // 'private' inheritance prevents such an access
    bs.push('a');
    // bs.push('b'); // error

    std::stack<char>* bug = new Bound_stack<char>(1); // 'private' inheritance prevents such an assignment
    bug->push('a');
    bug->push('b'); // Since 'push' is not virtual in 'Bound_stack' then 'push' from 'stack' (the type of 'bug') is called
    delete bug; // 'stack' destructor is called while 'bug' effectively points to a 'Bound_stack' object

    std::queue<char, std::list<char> > q;
    q.push('a');
    q.push('b');
    q.pop();
    std::cout << "front: " << q.front() << std::endl; // -> b
    // queue<char>::iterator i = q.begin();

    Hack* h = new Hack;
    h->send_message(); // 'Hack: let's bypass 'cypher'!' is displayed

    return 0;
}

