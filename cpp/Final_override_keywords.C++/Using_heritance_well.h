#ifndef _Using_heritance_well_h
#define _Using_heritance_well_h

#include <stack>

template <typename T> class Bound_stack : public std::stack<T> { // Default impl. for 'Allocator' is 'deque'
private:
    const typename std::stack<T>::size_type _capacity;
public:
    Bound_stack(const typename std::stack<T>::size_type&);
    bool full() const;
    void push(const T&);
};

template <typename T> Bound_stack<T>::Bound_stack(const typename std::stack<T>::size_type& capacity) : _capacity(capacity) {
}

template <typename T> bool Bound_stack<T>::full() const {
    return _capacity == this->size();
}

template <typename T> void Bound_stack<T>::push(const T& t) {
    if (full()) throw "push";
    std::stack<T>::push(t);
}

class Secret {
private:
    virtual void cypher();
public:
    void send_message();
};

class Hack : public Secret {
private:
    void cypher() override;
};


#endif
