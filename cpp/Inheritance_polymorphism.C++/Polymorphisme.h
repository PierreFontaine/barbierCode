#ifndef _Polymorphisme
#define _Polymorphisme

#include <map>
#include <vector>

class Compte_bancaire {
    int _id;
protected:
    float _solde;
    float _cumul_interets;
    Compte_bancaire(int, float = 0.F);
public:

    inline int id() const {
        return _id;
    }

    inline float solde() const {
        return _solde;
    }

    inline float cumul_interets() const {
        return _cumul_interets;
    }
    float mise_a_jour(float = 0.F);
    virtual float taux_interet() const = 0;
    virtual void appliquer_taux_interet();
};

class Compte_cheque : public Compte_bancaire {
protected:
    float _taux_interet;
    float _seuil;
public:
    Compte_cheque(int, float = 0.F, float = 0.F, float = 0.F);
    virtual float taux_interet() const;
    virtual void appliquer_taux_interet();
};

class Compte_epargne : public Compte_bancaire {
protected:
    Compte_epargne(int, float = 0.F);
};

class Compte_epargne_logement : public Compte_epargne {
public:
    const static float Taux_interet;
    Compte_epargne_logement(int, float = 0.F);
    virtual float taux_interet() const;
};

class Livret_A : public Compte_epargne {
public:
    const static float Taux_interet;
    Livret_A(int, float = 0.F);
    virtual float taux_interet() const;
};

class Client_de_banque {
    int _id;
protected:
    std::map<int, Compte_bancaire*> _comptes;
public:
    Client_de_banque();
    Client_de_banque(std::vector<Compte_bancaire*>&);
    float cumul_interets() const;

    inline int id() const {
        return _id;
    }
};

class Banque {
protected:
    std::map<int, Client_de_banque> _clients;
public:
    Banque(std::vector<Client_de_banque>&);
    float cumul_interets() const;
};

#endif
