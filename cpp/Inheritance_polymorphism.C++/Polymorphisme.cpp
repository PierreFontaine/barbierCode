#include <cassert>

#include "Polymorphisme.h"

Compte_bancaire::Compte_bancaire(int id, float solde) : _id(id), _solde(solde) {
}

float Compte_bancaire::mise_a_jour(float montant) {
    _solde += montant;
    return _solde;
}

void Compte_bancaire::appliquer_taux_interet() {
    _cumul_interets = _solde * (1.F + (taux_interet() / 100.F));
}

Compte_cheque::Compte_cheque(int id, float solde, float taux_interet, float seuil) : Compte_bancaire(id, solde), _taux_interet(taux_interet), _seuil(seuil) {
    assert(taux_interet >= 0.F && taux_interet < 100.F);
}

float Compte_cheque::taux_interet() const {
    return _taux_interet;
}

void Compte_cheque::appliquer_taux_interet() {
    if (_solde > _seuil) Compte_bancaire::appliquer_taux_interet();
}

Compte_epargne::Compte_epargne(int id, float solde) : Compte_bancaire(id, solde) {
}

Compte_epargne_logement::Compte_epargne_logement(int id, float solde) : Compte_epargne(id, solde) {
}
const float Compte_epargne_logement::Taux_interet = Livret_A::Taux_interet * 2.F / 3.F; // ne semble plus dépendre du Livret A ? à 0.75% depuis le 1er août 2009

float Compte_epargne_logement::taux_interet() const {
    return Taux_interet;
}

Livret_A::Livret_A(int id, float solde) : Compte_epargne(id, solde) {
}
const float Livret_A::Taux_interet = 1.25F; // 1er août 2009

float Livret_A::taux_interet() const {
    return Taux_interet;
}

Client_de_banque::Client_de_banque() {
}

Client_de_banque::Client_de_banque(std::vector<Compte_bancaire*>& comptes) {
    for (std::vector<Compte_bancaire*>::iterator i = comptes.begin(); i != comptes.end(); i++) _comptes[(*i)->id()] = *i;
}

float Client_de_banque::cumul_interets() const {
    float result = 0.F;
    for (std::map<int, Compte_bancaire*>::const_iterator i = _comptes.begin(); i != _comptes.end(); i++) result += (*i).second->cumul_interets();
    return result;
}

Banque::Banque(std::vector<Client_de_banque>& clients) {
    for (std::vector<Client_de_banque>::iterator i = clients.begin(); i != clients.end(); i++) _clients[(*i).id()] = *i;
}

float Banque::cumul_interets() const {
    float result = 0.F;
    for (std::map<int, Client_de_banque>::const_iterator i = _clients.begin(); i != _clients.end(); i++) result += (*i).second.cumul_interets();
    return result;
}
