#include <cstdlib>

#include <cassert>
#include <iostream>
#include <typeinfo>

#include "Note.h"
#include "Polymorphisme.h"

int main(int argc, char** argv) {
    Compte_bancaire* cb;
    Compte_cheque cc(1993, 10500.F, 2.F, 10000.F);
    Compte_epargne_logement cel(1996, 1000.F);

    Compte_bancaire& autre_cb = cel;
    autre_cb.appliquer_taux_interet(); // Polymorphism via reference instead of pointer

    cb = &cc;
    try {
        const std::type_info& ti = typeid (*cb);
        std::cout << ti.name() << '\n'; // 'Compte_cheque' is displayed
        if (ti == typeid (Compte_cheque)) std::cout << "Direct type: " << typeid (Compte_cheque).name() << '\n'; // 'Compte_cheque' is displayed
        if (ti == typeid (Compte_bancaire)) std::cout << "Indirect type: " << typeid (Compte_bancaire).name() << '\n'; // No display!
    } catch (std::bad_typeid& bti) {
        std::cout << "Error: " << bti.what() << '\n';
    }
    cb->appliquer_taux_interet();

    cb = &cel;
    cb->appliquer_taux_interet();

    std::cout << "cb: " << typeid (cb).name() << '\n'; // Pointer 'Compte_bancaire' is displayed
    std::cout << "*cb: " << typeid (*cb).name() << '\n'; // 'Compte_epargne_logement' is displayed

    try {
        Compte_cheque* autre_cc = static_cast<Compte_cheque*> (cb); // Bad idea!
        std::cout << "*autre_cc: " << typeid (*autre_cc).name() << '\n'; // 'Compte_epargne_logement' is displayed
        autre_cc = dynamic_cast<Compte_cheque*> (cb); // Bad idea as well, but less damage...
        std::cout << "*autre_cc: " << typeid (*autre_cc).name() << '\n'; // 'std::bad_typeid' exception raised!
    } catch (std::bad_typeid& bti) {
        std::cout << "Error: " << bti.what() << '\n';
    }

    // Compte_epargne_logement* autre_cel = (Compte_epargne_logement*)cb;
    Compte_epargne_logement* autre_cel = static_cast<Compte_epargne_logement*> (cb);
    assert(autre_cel != NULL);

    std::vector<Compte_bancaire*> comptes;
    comptes.push_back(&cc);
    comptes.push_back(&cel);
    Client_de_banque Franck(comptes);
    std::cout << "Interets Franck: " << Franck.cumul_interets() << '\n';

    std::vector<Client_de_banque> clients;
    clients.push_back(Franck);
    Banque b(clients);
    std::cout << "Interets banque: " << b.cumul_interets() << '\n';

    std::istringstream source("Franck Barbier");
    // Confidential_note* nic = new Note(&source); // Compilation error!
    // nic->write();

    Note* ni = new Confidential_note(&source);
    std::cout << ni->write() << '\n';
    delete ni;

    return 0;
}

