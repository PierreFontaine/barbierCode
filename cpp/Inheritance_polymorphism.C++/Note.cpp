#include <iostream>

#include "Note.h"

int Note::_Increment = 10;
const std::string Note::Default_header = "Note: ";

void Note::initialization() {
    _text = Default_header;
}

Note::Note(std::istringstream * const source) : _source(source) {
    initialization();
}

Note::~Note() {
}

void Note::collect_data() {
    char c;
    while (_source->good()) {
        _source->get(c);
        if (!_source->eof()) _text += c;
    }
}

std::string Note::write() {
    collect_data();
    return _text;
}

const std::string Confidential_note::Default_header = "Confidential note: ";

void Confidential_note::initialization() {
    _text = Default_header;
}

Confidential_note::Confidential_note(std::istringstream * const source) : Note(source) {
    initialization();
}

std::string Confidential_note::write() {
    return Note::write() + " {Nobody should read it...}";
}

const std::string Top_secret_note::Default_header = "Top secret note: ";

void Top_secret_note::initialization() {
    _text = Default_header;
}

Top_secret_note::Top_secret_note(std::istringstream * const source) : Confidential_note(source) {
    initialization();
}

std::string Top_secret_note::write() {
    std::string cn = Confidential_note::write();
    return encrypt(cn) + " {Unreadable!}";
}

std::string Top_secret_note::encrypt(std::string& message) {
    for (std::string::iterator i = message.begin(); i != message.end(); i++) *i += '$';
    return message;
}
