#include <iostream>
#include <string>

#include "Simple_genericity.h"

class Elephant {
  std::string _name;
public:
  inline Elephant(std::string name) : _name(name) {}
  inline std::string asString() const {return _name;}
};

int main(int argc, char** argv) {
  Elephant Babar("Babar"),Jumbo("Jumbo");
  LIFO<Elephant> zoo;

  zoo.in(Babar);
  zoo.in(Jumbo);
  std::cout << "Jumbo goes out since it is the last-in: " << zoo.out().asString() << std::endl;

  return 0;
}