#ifndef _SIMPLE_GENERICITY_H
#define	_SIMPLE_GENERICITY_H

#include <list>

template <typename T> class LIFO {
private:
    std::list<T> _representation;
public:
    bool empty() const;
    void in(const T&);
    T out(); // This returns a copy!
};

template <typename T> bool LIFO<T>::empty() const {
    return _representation.empty();
}

template <typename T> T LIFO<T>::out() {
    if (empty()) throw "empty...";
    T t = _representation.back();
    _representation.pop_back();
    return t;
}

template <typename T> void LIFO<T>::in(const T& t) {
    _representation.push_back(t);
}

#endif	/* _SIMPLE_GENERICITY_H */

