#include <iostream>

#include "Lbs.h"
#include "Message.h"
#include "Ubs.h"

using namespace std;

int main(int argc, char** argv) {
    char* buffer = new char[Limited_bit_stream::Capacity + 1];

    Limited_bit_stream lbs0, lbs1("0000"), lbs2("00001111");

    cout << "nothing:" << lbs0.format(buffer) << "\n\n";
    cout << lbs1.format(buffer) << "\n\n";
    cout << lbs2.format(buffer) << "\n\n";

#ifdef _DEBUG
    cout << "lbs2[0] equals to 1? ..." << lbs2[Limited_bit_stream::minindex()] << "\n\n";
    cout << "lbs2[3] equals to 1? ..." << lbs2[3] << "\n\n";
    cout << "lbs2[4] equals to 0? ..." << lbs2[4] << "\n\n";
#endif  

    lbs1(0, lbs2[0]);
    cout << lbs1.format(buffer) << "\n\n"; // 0001
    lbs1(3, lbs2[0]);
    cout << lbs1.format(buffer) << "\n\n"; // 1001

    Limited_bit_stream lbs3 = lbs1 + lbs2;
    cout << lbs3.format(buffer) << "\n\n"; // 100100001111  
    lbs3 = lbs3 + lbs1;
    cout << lbs3.format(buffer) << "\n\n"; // 1001000011111001  
    lbs3 += lbs1;
    cout << lbs3.format(buffer) << "\n\n"; // 10010000111110011001

    char a_string[] = {'0', '1', '\0'};
    Limited_bit_stream lbs4(a_string);
    cout << lbs4.format(buffer) << "\n\n"; // 01 

    delete[] buffer;

    char ubs_buffer[100];

    Unlimited_bit_stream ubs0, ubs1("1111111111111111111111111111111100000000");
    cout << ubs0.format(ubs_buffer) << '(' << ubs0.size() << ')' << "\n\n";
    cout << ubs1.format(ubs_buffer) << '(' << ubs1.size() << ')' << "\n\n";

    Unlimited_bit_stream ubs2 = ubs1;
    cout << ubs2.format(ubs_buffer) << '(' << ubs2.size() << ')' << "\n\n";

#ifdef _DEBUG
    cout << "ubs2[0] equals to 0? ..." << ubs2[Unlimited_bit_stream::minindex()] << "\n\n";
    cout << "ubs2[7] equals to 0? ..." << ubs2[7] << "\n\n";
    cout << "ubs2[8] equals to 1? ..." << ubs2[8] << "\n\n";
    cout << "ubs2[39] equals to 1? ..." << ubs2[39] << "\n\n";
#endif

    Unlimited_bit_stream ubs3 = ubs1 + Unlimited_bit_stream("0101");
    cout << ubs3.format(ubs_buffer) << '(' << ubs3.size() << ')' << "\n\n";
    ubs3 += ubs1;
    cout << ubs3.format(ubs_buffer) << '(' << ubs3.size() << ')' << "\n\n";

#ifdef _DEBUG
    cout << "ubs3[0] equals to: " << ubs3[Unlimited_bit_stream::minindex()] << "\n\n";
    cout << "ubs3[40] equals to: " << ubs3[40] << "\n\n";
    cout << "ubs3[41] equals to: " << ubs3[41] << "\n\n";
    cout << "ubs3[42] equals to: " << ubs3[42] << "\n\n";
#endif

    buffer = new char[Message::Capacity + 1];

    Limited_bit_stream xlbs1, xlbs2("0000000000000001"), xlbs3("0111111111111110");
    Unlimited_bit_stream xubs1, xubs2("0000000000000001"), xubs3("1111111111111110");
    Message x1(xlbs1), x2(xlbs2), x3(xlbs3), x4(xubs1), x5(xubs2), x6(xubs3);
    cout << "x1: " << x1.format(buffer) << '\n';
    cout << "x2: " << x2.format(buffer) << '\n';
    cout << "x3: " << x3.format(buffer) << '\n';
    cout << "x4: " << x4.format(buffer) << '\n';
    cout << "x5: " << x5.format(buffer) << '\n';
    cout << "x6: " << x6.format(buffer) << "\n\n";

    x1 = x3 + x2;
    x4 += x5;
    cout << "x1: " << x1.format(buffer) << '\n';
    cout << "x4: " << x4.format(buffer) << "\n\n";

    Message* x7 = new Message(x6), *x8;
    x8 = &x3;
    x3 = *x7;
    cout << "x7: " << x7->format(buffer) << '\n';
    cout << "x8: " << x8->format(buffer) << '\n';
    delete x7;
    cout << "x3: " << x3.format(buffer) << "\n\n";

    delete[] buffer;

    return 0;
}

