#ifndef _Ubs_h
#define _Ubs_h

#include "Lbs.h"

class Unlimited_bit_stream {
public:
    static unsigned int minindex();

protected:
    Limited_bit_stream* _buffer;
    unsigned int _count;

public:
    Unlimited_bit_stream();
    Unlimited_bit_stream(const char[]);
    Unlimited_bit_stream(const Limited_bit_stream&);
    Unlimited_bit_stream(const Unlimited_bit_stream&);
    virtual ~Unlimited_bit_stream();

    virtual char* format(char[]) const;
    virtual unsigned int size() const;
    virtual Limited_bit_stream::Bit operator[](unsigned int) const; // Get bit
    // virtual Bit& operator [] (unsigned int); // Get + set bit
    virtual void operator()(unsigned int, Limited_bit_stream::Bit); // Set bit
    virtual Unlimited_bit_stream operator+(const Unlimited_bit_stream&) const;
    virtual Unlimited_bit_stream& operator+=(const Unlimited_bit_stream&);
    virtual const Unlimited_bit_stream& operator=(const Unlimited_bit_stream&);
};

#endif
