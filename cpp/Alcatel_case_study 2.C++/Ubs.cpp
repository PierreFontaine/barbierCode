#include <cassert>
#include <cstring>

#include "Ubs.h"

unsigned int Unlimited_bit_stream::minindex() {
    return Limited_bit_stream::minindex();
}

Unlimited_bit_stream::Unlimited_bit_stream() {
    _buffer = NULL;
    _count = 0;
}

Unlimited_bit_stream::Unlimited_bit_stream(const char a_string[]) {
    _buffer = NULL;
    _count = 0;
    unsigned int length = std::strlen(a_string);
    if (length) {
        if (length % Limited_bit_stream::Capacity)
            _buffer =
                new Limited_bit_stream[_count = length / Limited_bit_stream::Capacity + 1];
        else
            _buffer =
                new Limited_bit_stream[_count = length / Limited_bit_stream::Capacity];
        assert(_count > 0);
        char* buffer = new char[Limited_bit_stream::Capacity + 1];
        buffer[Limited_bit_stream::Capacity] = '\0';
        unsigned int index;
        for (index = 0; index < _count; index++) {
            (void) std::strncpy(buffer, &a_string[index * Limited_bit_stream::Capacity], Limited_bit_stream::Capacity);
            _buffer[index] = Limited_bit_stream(buffer);
        }
        delete[/* Limited_bit_stream::Capacity + 1 */] buffer;
    }
}

Unlimited_bit_stream::Unlimited_bit_stream(const Limited_bit_stream& a_Limited_bit_stream) {
    _buffer = new Limited_bit_stream[1];
    _buffer[0] = a_Limited_bit_stream;
    _count = 1;
}

Unlimited_bit_stream::Unlimited_bit_stream(const Unlimited_bit_stream& an_Unlimited_bit_stream) {
    _buffer = new Limited_bit_stream[_count = an_Unlimited_bit_stream._count];
    unsigned int index;
    for (index = 0; index < an_Unlimited_bit_stream._count; index++) {
        _buffer[index] = an_Unlimited_bit_stream._buffer[index];
    }
}

Unlimited_bit_stream::~Unlimited_bit_stream() {
    if (_buffer) delete[] _buffer;
}

char* Unlimited_bit_stream::format(char a_string[]) const {
    if (!size()) a_string[0] = '\0';
    else {
        char* buffer = new char[Limited_bit_stream::Capacity + 1];
        buffer[Limited_bit_stream::Capacity] = '\0';
        unsigned int index;
        for (index = 0; index < _count; index++) {
            (void) std::strcpy(&a_string[index * Limited_bit_stream::Capacity],
                    _buffer[index].format(buffer));
        }
        delete[/* Limited_bit_stream::Capacity + 1 */] buffer;
    }
    return a_string;
}

unsigned int Unlimited_bit_stream::size() const {
    unsigned int length = 0;
    unsigned int index;
    for (index = 0; index < _count; index++)
        length += _buffer[index].size();
    return length;
}

Limited_bit_stream::Bit Unlimited_bit_stream::operator[](unsigned int index) const {
    assert(index < size());
    int subindex = ((int) size() - 1 - (int) index) / (int) Limited_bit_stream::Capacity;
    assert(subindex >= 0 && subindex < (int) _count);
    int position = (int) _buffer[subindex].size() - (((int) size() - 1 - (int) index) % (int) Limited_bit_stream::Capacity) - 1;
    assert(position >= 0 && position < (int) _buffer[subindex].size());
    return _buffer[subindex].operator[](position);
}

void Unlimited_bit_stream::operator()(unsigned int index, Limited_bit_stream::Bit a_Bit) {
    assert(index < size());
    int subindex = ((int) size() - 1 - (int) index) / (int) Limited_bit_stream::Capacity;
    assert(subindex >= 0 && subindex < (int) _count);
    int position = (int) _buffer[subindex].size() - (((int) size() - 1 - (int) index) % (int) Limited_bit_stream::Capacity) - 1;
    assert(position >= 0 && position < (int) _buffer[subindex].size());
    _buffer[subindex].operator()(position, a_Bit);
}

Unlimited_bit_stream Unlimited_bit_stream::operator+(const Unlimited_bit_stream& an_Unlimited_bit_stream) const {
    char* buffer1 = new char[size() + an_Unlimited_bit_stream.size() + 1];
    buffer1[size() + an_Unlimited_bit_stream.size()] = '\0';
    char* buffer2 = new char[an_Unlimited_bit_stream.size() + 1];
    buffer2[an_Unlimited_bit_stream.size()] = '\0';
    Unlimited_bit_stream ubs(std::strcat(format(buffer1), an_Unlimited_bit_stream.format(buffer2)));
    delete[/* an_Unlimited_bit_stream.size() + 1 */] buffer2;
    delete[/* size() + an_Unlimited_bit_stream.size() + 1 */] buffer1;
    return ubs;
}

Unlimited_bit_stream& Unlimited_bit_stream::operator+=(const Unlimited_bit_stream& an_Unlimited_bit_stream) {
    *this = *this +an_Unlimited_bit_stream;
    return *this;
}

const Unlimited_bit_stream& Unlimited_bit_stream::operator=(const Unlimited_bit_stream& an_Unlimited_bit_stream) {
    if (this != &an_Unlimited_bit_stream) {
        if (_buffer) delete[] _buffer;
        _buffer = new Limited_bit_stream[_count = an_Unlimited_bit_stream._count];
        unsigned int index;
        for (index = 0; index < an_Unlimited_bit_stream._count; index++) {
            _buffer[index] = an_Unlimited_bit_stream._buffer[index];
        }
    }
    return *this;
}

