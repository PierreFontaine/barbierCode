#include "Mixing_heritance_genericity.h"

Vegetable::Vegetable_colors Vegetable::color() const {
    return Vegetable_colors::Green; // Default color...
}

Vegetable::Vegetable_colors Carot::color() const {
    return Vegetable_colors::Orange;
}

int main(int argc, char** argv) {
    Carot a_carot;

    My_garden<Carot> a_garden(a_carot);

    //    My_garden<Carot>& another_garden = a_garden; // This works (as expected!)...
    My_garden<Vegetable>& another_garden = a_garden; // This DOES NOT work (as expected!)...

    return 0;
}

