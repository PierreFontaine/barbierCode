#ifndef _Mixing_heritance_genericity_h
#define _Mixing_heritance_genericity_h

template <typename T> class My_garden {
private:
    T _t;
public:
    My_garden(const T&);
};

template <typename T> My_garden<T>::My_garden(const T& t) : _t(t) {
}

class Vegetable {
public:

    enum Vegetable_colors {
        Green, Orange, Purple, Red
    };
    virtual Vegetable_colors color() const;
};

class Carot : public Vegetable {
public:
    Vegetable_colors color() const;
};

#endif
