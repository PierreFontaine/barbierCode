#ifndef _Expression_h
#define _Expression_h

#include <cstring>

class Expression {
protected:
    char* _implementation;
public:
    Expression();
    Expression(char*);
    Expression(const Expression&);
    virtual ~Expression();
    virtual inline int size() const;
    virtual const char* format() const;
    const Expression& operator=(const Expression&);
};

int Expression::size() const {
    return std::strlen(_implementation);
}

#endif
