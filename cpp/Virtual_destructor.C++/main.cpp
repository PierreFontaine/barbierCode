#include <iostream>

#include "Sentence.h"

int main() {
    Expression s0 = (char*) "Sophie", *s1, s2 = (char*) "Oscar Barbier--Darnal", s3 = (char*) "Léna Barbier--Darnal"; // // Literal Expressions have 'const char*' type from C++11
    char separators1[] = {' ', '-'};
    Sentence* x1 = new Sentence((char*) "Franck Barbier", separators1);
    Sentence x2 = *x1;
    s1 = x1;
    delete s1;
    std::cout << s0.format() << ':' << s0.size() << '\n'; // Sophie: 6    
    std::cout << x2.format() << ':' << x2.size() << '\n'; // Franck Barbier: 2 
    std::cout << s2.format() << ':' << s2.size() << '\n'; // Oscar Barbier--Darnal: 22
    std::cout << s3.format() << ':' << s3.size() << '\n'; // Léna Barbier--Darnal: 20
    char separators2[] = {' '};
    Sentence x3(s2, separators2), x4(s2, separators1);
    s1 = &x3;
    s3 = x2;
    std::cout << s1->format() << ':' << s1->size() << '\n'; // Oscar Barbier--Darnal: 2
    std::cout << s3.format() << ':' << s3.size() << '\n'; // Franck Barbier: 14
    std::cout << x4.format() << ':' << x4.size() << '\n'; // Oscar Barbier--Darnal: 3
    
    Expression* e = new Sentence((char*) "Franck Barbier", separators1);
    
    delete e;
    
    
    return 0;
}
