#include <cstdlib> // nullptr

#include "Sentence.h"

bool Sentence::separator(char c) const {
    int i = 0;
    bool is_separator = false;
    while (_separator_list[i]) {
        if (_separator_list[i] == c) {
            is_separator = true;
            break;
        }
        i++;
    }
    return is_separator;
}

void Sentence::initialize(const char sl[]) {
    int sl_length = std::strlen(sl);
    if (sl_length) {
        _separator_list = new char[sl_length + 1];
        _separator_list[sl_length] = '\0';
        for (; sl_length--; sl++) _separator_list[sl_length] = *sl;
    } else _separator_list = nullptr;
}

Sentence::Sentence(const char sl[]) : Expression() {
    initialize(sl);
}

Sentence::Sentence(char* s, const char sl[]) : Expression(s) {
    initialize(sl);
}

Sentence::Sentence(const Expression& s, const char sl[]) : Expression(s) {
    initialize(sl);
}

Sentence::Sentence(const Sentence& s) : Expression(s) {
    initialize(s._separator_list);
}

Sentence::~Sentence() {
    if (_separator_list) delete[] _separator_list;
}

int Sentence::size() const {
    int word_number = 0;
    if (Expression::size()) {
        char* index1 = _implementation, *index2 = _implementation;
        for (; *index1; index2 = index1++) {
            if (separator(*index1) && !separator(*index2)) word_number++;
        }
        if (!separator(*index2)) word_number++;
    }
    return word_number;
}

int Sentence::length() const {
    return Expression::size();
}

const Sentence& Sentence::operator=(const Sentence& s) {
    if (this != &s) this->Expression::operator=(s);
    return *this;
}

