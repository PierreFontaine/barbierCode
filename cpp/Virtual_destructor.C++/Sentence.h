#ifndef _Sentence_h
#define _Sentence_h

#include "Expression.h"

class Sentence : public Expression {
private:
    char* _separator_list;
    bool separator(char) const;
    void initialize(const char[]);
public:
    Sentence(const char[]);
    Sentence(char*, const char[]);
    Sentence(const Expression&, const char[]);
    Sentence(const Sentence&);
    ~Sentence(); // Not virtual: we do not inherit from 'Sentence'	
    int size() const;
    int length() const;
    const Sentence& operator=(const Sentence&);
};

#endif
