#include <cstring>

#include "Expression.h"

Expression::Expression() {
    _implementation = new char[1];
    _implementation[0] = 0; // End
}

Expression::Expression(char* s) {
    _implementation = new char[std::strlen(s) + 1];
    std::strcpy(_implementation, s);
}

Expression::Expression(const Expression& s) {
    _implementation = new char[std::strlen(s._implementation) + 1];
    std::strcpy(_implementation, s._implementation);
}

Expression::~Expression() {
    if (_implementation) delete[] _implementation;
}

const char* Expression::format() const {
    return _implementation;
}

const Expression& Expression::operator=(const Expression& s) {
    if (this != &s) {
        if (_implementation) delete[] _implementation;
        this->_implementation = new char[std::strlen(s._implementation) + 1];
        std::strcpy(this->_implementation, s._implementation);
    }
    return *this;
}
