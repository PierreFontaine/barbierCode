#include <iostream>

#include "Friend.h"

void A::f(const B& b) const {
    b._i;
}

Expression::Expression(const std::string& s) : _content(s) {
}

std::string Expression::_read() const {
    return _content;
}

Phrase::Phrase(const std::string& s) : _content(s) {
}

Phrase operator+(const Phrase& p, const Expression& e) {
    Phrase result(p._content + e._read()); // Access to properties of 'Expression' and 'Phrase'
    return result;
}

std::string Reader::read(const Expression& e) const { // Access to private properties of 'Expression'
    return e._content;
}

std::string Reader::read(const Phrase& p) const { // Access to private properties of 'Phrase'
    return p._content;
}

int main(int argc, char** argv) {
    Reader FranckBarbier;
    Expression e("Caution! Avoid it...");
    Phrase p("The 'friend' keyword... ");
    std::cout << "e: " << FranckBarbier.read(e) << '\n';
    std::cout << "p: " << FranckBarbier.read(p) << '\n';
    std::cout << "p + e: " << FranckBarbier.read(operator+(p, e)) << '\n';

    return 0;
}

