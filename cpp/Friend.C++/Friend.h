#ifndef _Friend_h
#define _Friend_h

#include <string>

class B;

class A {
public:
    void f(const B&) const;
};

class B {
    int _i;
    friend void A::f(const B&) const;
};

class Reader;

class Phrase;

class Expression {
    std::string _content;
public:
    Expression(const std::string&);
private:
    std::string _read() const;
    friend Phrase operator+(const Phrase&, const Expression&); // Here, the '+' operator is not a member function
    friend class Reader;
};

class Phrase {
    const std::string _content;
public:
    Phrase(const std::string&);
    friend Phrase operator+(const Phrase&, const Expression&); // Here, the '+' operator is not a member function
    friend class Reader;
};

class Reader {
public:
    std::string read(const Expression&) const;
    std::string read(const Phrase&) const;
};

#endif
