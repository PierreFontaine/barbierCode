#include <cstdio>
#include <ctime>

int main(int argc, char** argv) {

    std::time_t t = 0L; // One refers to the 'long' primtive type, which means "non-encapsulation" -> very bad idea!
    std::printf(std::asctime(std::gmtime(&t))); // January 1, 1970, 00:00:00 GMT
    t = -1L; // That's the reason why encapsulation is important. Doing this is permissive -> again, very bad idea!
    std::printf(std::asctime(std::gmtime(&t))); // This may work, but...

    return 0;
}

