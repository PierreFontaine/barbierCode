#include "Job.h"

const std::string Job::Undefined_identifier = "undefined job identifier";

Job::Job() : _identifier(Undefined_identifier) {
}

Job::Job(const std::string identifier) : _identifier(identifier) {
}

const std::string Job::identifier() const {
    return _identifier;
}

bool Job::operator<(const Job& j) const {
    return _identifier < j._identifier; // Alphabetical order
}
