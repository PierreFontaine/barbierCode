#ifndef _Job_h
#define _Job_h

#include <string>

class Job {
public:
    static const std::string Undefined_identifier;
private:
    std::string _identifier;
public:
    Job();
    Job(const std::string);
    const std::string identifier() const;
    bool operator<(const Job&) const; // Mandatory for 'Job' as 'Value' type of 'map'
};

#endif

