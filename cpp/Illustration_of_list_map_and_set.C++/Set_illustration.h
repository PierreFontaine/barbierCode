#ifndef _Set_h
#define _Set_h

#include <algorithm>
#include <set>

template <typename T> class Set : public std::set<T> {
public:
    bool strict_subset(const Set<T>&);
    Set<T> operator-(const Set<T>&);
};

template<typename T> bool Set<T>::strict_subset(const Set<T>& s) {
    Set<T> intersection;
    std::set_intersection(this->begin(), this->end(), s.begin(), s.end(), std::inserter(intersection, intersection.end()));
    return intersection == *this && intersection != s;
}

template<typename T> Set<T> Set<T>::operator-(const Set<T>& s) {
    Set<T> difference;
    std::set_difference(this->begin(), this->end(), s.begin(), s.end(), std::inserter(difference, difference.end()));
    return difference;
}

#endif
