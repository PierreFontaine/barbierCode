#include <cassert>
#include <cstdlib>
#include <ctime>

#include "Task.h"

Task::Task() {
    std::srand(std::time(&_start));
    do {
        std::srand(std::time(&_end));
    } while (_end <= _start);
    assert(_start < _end);
}

Task::Task(const std::time_t start, const std::time_t end) : _start(start), _end(end) {
    assert(_start < _end);
}

const std::string Task::start() const {
    return std::string(std::asctime(std::gmtime(&_start)));
}

const std::string Task::end() const {
    return std::string(std::asctime(std::gmtime(&_end)));
}

bool Task::operator<(const Task& t) const {
    return _start < t._start;
}

Task Task::operator+(const Task& t) const {
    return Task(_start < t._start ? _start : t._start, _end < t._end ? t._end : _end);
}
