#ifndef _Task_h
#define _Task_h

#include <ctime>
#include <string>

class Task {
private:
    std::time_t _start;
    std::time_t _end;
public:
    Task();
    Task(const std::time_t, const std::time_t);
    const std::string start() const;
    const std::string end() const;
    // bool operator ==(const Task&) const; // for 'std::find' and 'std::unique' generic functions
    bool operator<(const Task&) const; // for 'std::less<Task>'
    Task operator+(const Task&) const; // for 'std::accumulate' generic function
};

#endif
