#include <algorithm> // STL 'std::binary_search' generic function
#include <cassert>
#include <iostream>
#include <map>
#include <list>
#include <set>

#include "Job.h"
#include "Set_illustration.h"
#include "Task.h"

int main(int argc, char** argv) {
    Set<char> E, F;
    E.insert('a');
    F.insert('a');
    F.insert('b');
    std::cout << "E strictly included in F? " << E.strict_subset(F) << std::endl; // 'true'
    E.insert('b');
    std::cout << "E strictly included in F? " << E.strict_subset(F) << std::endl; // 'false'
    F.erase('b');
    Set<char> difference = E - F;
    for (Set<char>::iterator i = difference.begin(); i != difference.end(); i++) std::cout << *i << std::endl;

    std::set<char> alphabet;
    alphabet.insert('a');
    alphabet.insert('a');
    assert(alphabet.size() == 1);

    short tab[] = {3, 4, 6, 7, 8, 11, 12, 13};
    std::set<short> set(tab, tab + 8);
    assert(std::binary_search(set.begin(), set.end(), 13)); // '#include <algorithm>'
    assert(!std::binary_search(set.begin(), set.end(), 14)); // '#include <algorithm>'

    time_t start = 1000000L;
    time_t end = 2000000L;
    Task t1, t2, t3, t4(start, end), t5(start, end);

    Job j1("j1"), j2("j2");

    std::map<Task, Job, std::less<Task> > scheduler; // 'std::less_equal<Task>' cannot work, see: STL "Strict Weak Order"
    assert(scheduler.empty());
    scheduler[t1] = j1;
    scheduler[t1] = j2;
    std::cout << "'scheduler.count(t1) == 1'? " << scheduler.count(t1) << '\n';
    std::cout << "At t1, there is what? " << (*scheduler.find(t1)).second.identifier().c_str() << '\n';
    scheduler[t2] = j1;
    std::cout << "At t2, there is what? " << ((*scheduler.find(t2)).second.identifier()).c_str() << '\n';
    assert(scheduler.size() == 2);
    (void) scheduler[t3];
    assert(scheduler.size() == 3);
    std::map<Task, Job, std::less<Task> >::iterator i = scheduler.find(t3);
    std::cout << "At t3, there is what? " << ((*i).second.identifier()).c_str() << ": " << (*i).first.start();
    std::cout << "How many t3 are removed? " << scheduler.erase(t3) << '\n';
    assert(scheduler.size() == 2);

    std::multimap<Task, Job, std::less<Task> > another_scheduler;
    std::pair<Task, Job> p1(t1, j2); // 'another_scheduler[t1] = j2;' -> cannot work for 'multimap' because '[]' not redefined
    another_scheduler.insert(p1);
    std::pair<Task, Job> p2(t2, j1); // 'another_scheduler[t2] = j1;'
    another_scheduler.insert(p2);
    std::pair<Task, Job> p3(t3, j2); // 'another_scheduler[t3] = j2;'
    another_scheduler.insert(p3);
    std::pair<Task, Job> p4(t4, j2); // 'another_scheduler[t4] = j2;'
    another_scheduler.insert(p4);
    std::pair<Task, Job> p5(t5, j1); // 'another_scheduler[t5] = j1;'
    another_scheduler.insert(p4);
    assert(another_scheduler.size() == 5); // Even though '! (t4 < t5)', 'p5' does not replace 'p4' because doublons in 'multimap'
    assert(std::includes(another_scheduler.begin(), another_scheduler.end(), scheduler.begin(), scheduler.end()));
    std::multimap<Task, Job, std::less<Task> >::iterator min = std::min_element(another_scheduler.begin(), another_scheduler.end());
    std::cout << "early job and associated start time (another scheduler): " << ((*min).second.identifier()).c_str() << ": " << (*min).first.start();
    std::multimap<Task, Job, std::less<Task> >::iterator max = std::max_element(another_scheduler.begin(), another_scheduler.end());
    std::cout << "late job and associated start time (another scheduler): " << ((*max).second.identifier()).c_str() << ": " << (*max).first.start();
    for (std::multimap<Task, Job, std::less<Task> >::iterator j = another_scheduler.begin(); j != another_scheduler.end(); j++) std::cout << (*j).second.identifier() << ": " << (*j).first.start();

    std::cout << std::endl;
    std::list<const Task*> l;
    assert(l.empty());
    l.push_front(&t1); // t1
    l.push_front(&t2); // t2 t1
    l.push_front(&t3); // t3 t2 t1
    l.push_front(&t4); // t4 t3 t2 t1
    l.insert(l.begin(), &t5); // t5 t4 t3 t2 t1
    l.push_back(&t1); // t5 t4 t3 t2 t1 t1
    assert(l.size() == 6);

    std::list<const Task*>::iterator j;
    for (j = l.begin(); !(j == l.end()); j++) std::cout << "- " << (*j)->start() << '\t';
    std::cout << std::endl;

    std::list<const Task*>::iterator where_is_t2 = std::find(l.begin(), l.end(), &t2);
    std::cout << "Task 't2': " << (*where_is_t2)->start() << std::endl;

    std::reverse(l.rbegin(), l.rend()); // t1 t1 t2 t3 t4 t5
    for (j = l.begin(); !(j == l.end()); j++) std::cout << "- " << (*j)->start() << '\t';
    std::cout << std::endl;

    std::list<const Task*>::reverse_iterator k;
    for (k = l.rbegin(); !(k == l.rend()); k++) std::cout << "- " << (*k)->start() << '\t';
    std::cout << std::endl;

    l.sort(); // Pointers order: t5 t4 t3 t2 t1 t1
    for (j = l.begin(); !(j == l.end()); j++) std::cout << "- " << (*j)->start() << '\t';
    std::cout << std::endl;

    l.unique(); // t5 t4 t3 t2 t1 (remove only consecutive elements?) 
    for (j = l.begin(); !(j == l.end()); j++) std::cout << "- " << (*j)->start() << '\t';
    std::cout << std::endl;
    return 0;
}

