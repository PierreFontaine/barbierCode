#include <cstdio> // 'std::sprintf', etc.
#include <cstdlib> // 'std::srand', etc.
#include <iostream>

#include "EM.h"

Invalid_temperature_exception::Invalid_temperature_exception(float value) : _value(value) {
}

float Invalid_temperature_exception::value() const {
    return _value;
}

const char* Invalid_temperature_exception::what() const throw () {
    return "Invalid temperature";
}

const float Temperature::Min = -273.15F;

Temperature::Temperature() {
    _value = 0.F;
    _step = 0.0001F;
}

Temperature::Temperature(float value, enum Temperature_unit unit) throw (char*, Invalid_temperature_exception/*, std::bad_exception*/) {
    switch (unit) {
        case Celsius: _value = value;
            break;
        case Fahrenheit: _value = (value - 32.F) * 5.F / 9.F;
            break;
        case Kelvin: _value = value + Min;
            break;
        default: throw "Illegal temperature unit";
    }
    if (_value < Min) throw Invalid_temperature_exception(_value);
    _step = 0.0001F;
}

float Temperature::asCelsius() const {
    return _value;
}

float Temperature::asFahrenheit() const {
    return _value * 9.F / 5.F + 32.F;
}

float Temperature::asKelvin() const {
    return _value - Min;
}

void Temperature::increment() throw () {
    _value += _step;
}

void Temperature::decrement() throw (float, std::bad_exception) {
    _value -= _step;
    if (_value < Min) throw _value;
}

int Temperature::operator<(const Temperature& t) const {
    return (_value < t._value) ? true : false;
}

int Temperature::operator<=(const Temperature& t) const {
    return _value <= t._value ? true : false;
}

int Temperature::operator>(const Temperature& t) const {
    return _value > t._value ? true : false;
}

int Temperature::operator>=(const Temperature& t) const {
    return _value >= t._value ? true : false;
}

void Global_exception_management::my_unexpected() {
    std::cout << "my_unexpected" << std::endl; // Partial exception processing
    throw; // This creates a chain so that an instance of 'std::bad_exception' is thrown whether the failing function has this type in its signature
}

void Global_exception_management::my_terminate() {
    std::cout << "my_terminate" << std::endl; // Ror test only
}

Invalid_temperature_switching_exception::Invalid_temperature_switching_exception(enum Temperature::Temperature_unit invalid_temperature_mode) {
    _invalid_temperature_mode = invalid_temperature_mode;
}

enum Temperature::Temperature_unit Invalid_temperature_switching_exception::invalid_temperature_mode() const {
    return _invalid_temperature_mode;
}

Temperature Programmable_thermostat::temperature() const {
    Temperature ambient_temperature = target_temperature;
    std::time_t timer = std::time(&timer);
    std::srand((unsigned int) timer);
    if (std::rand() % 2) ambient_temperature.increment();
    else {
        try {
            ambient_temperature.decrement();
        } catch (Invalid_temperature_exception) {
            ambient_temperature.increment();
        }
    }
    return ambient_temperature;
}

Programmable_thermostat::Programmable_thermostat() : target_temperature(50.F, Temperature::Fahrenheit) {
    _time = std::time(&_time);
    _temperature_mode = Temperature::Fahrenheit;
}

void Programmable_thermostat::f_c() throw (Invalid_temperature_switching_exception) {
    if (_temperature_mode == Temperature::Fahrenheit) _temperature_mode = Temperature::Celsius;
    else if (_temperature_mode == Temperature::Celsius) _temperature_mode = Temperature::Fahrenheit;
    else throw Invalid_temperature_switching_exception(_temperature_mode);
}

bool Programmable_thermostat::temp_down() {
    if (target_temperature > Temperature(40.F, Temperature::Fahrenheit)) {
        try {
            target_temperature.decrement();
        } catch (Invalid_temperature_exception) {
            target_temperature.increment();
            return false;
        }
        return true;
    }
    return false;
}

bool Programmable_thermostat::temp_up() {
    if (target_temperature < Temperature(90.F, Temperature::Fahrenheit)) {
        target_temperature.increment();
        return true;
    }
    return false;
}

void Programmable_thermostat::time_backward() {
    _time -= 1L;
}

void Programmable_thermostat::time_forward() {
    _time += 1L;
}

std::string Programmable_thermostat::printable_time() const {
    return std::ctime(&_time);
}

std::string Programmable_thermostat::printable_temperature() const {
    int source;
    if (_temperature_mode == Temperature::Fahrenheit)
        source = (int) temperature().asFahrenheit();
    else source = (int) temperature().asCelsius();
    char buffer[BUFSIZ];
    (void) std::sprintf(buffer, "%d", source);
    std::string result(buffer);
    if (_temperature_mode == Temperature::Fahrenheit) result += "°F";
    else result += "°C";
    return result;
}

std::string Programmable_thermostat::printable_target_temperature() const {
    int source;
    if (_temperature_mode == Temperature::Fahrenheit)
        source = (int) target_temperature.asFahrenheit();
    else source = (int) target_temperature.asCelsius();
    char buffer[BUFSIZ];
    (void) std::sprintf(buffer, "%d", source);
    std::string result(buffer);
    if (_temperature_mode == Temperature::Fahrenheit) result += "°F";
    else result += "°C";
    return result;
}
