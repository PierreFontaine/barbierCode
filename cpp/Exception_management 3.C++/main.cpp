#include <cstdlib>

#include <iostream>

#include "EM.h"

int main(int argc, char** argv) {
    std::set_unexpected(&Global_exception_management::my_unexpected);
    std::set_terminate(&Global_exception_management::my_terminate);

    std::cout << "Absolute zero: " << Temperature::Min << std::endl;

    try {
        Temperature t(0.F, Temperature::Kelvin); // Instance of 'Invalid_temperature_exception' may be raised or "Illegal temperature unit"
        t.decrement(); // 'float' may be raised
    } catch (void* e) { // Does not match either 'float' nor 'Invalid_temperature_exception' nor "Illegal temperature unit"
        std::cerr << "double: " << e << std::endl;
    } catch (char* e) { // Matches "Illegal temperature unit"
        std::cerr << e << std::endl;
    } catch (Invalid_temperature_exception& ite) {
        std::cerr << ite.what() << std::endl;
    } catch (std::bad_exception& be) { // 'Global_exception_management::my_unexpected' is called before
        std::cerr << "be: " << be.what() << std::endl;
    } catch (...) {
        std::cerr << "Default management" << std::endl;;
    }

    try {
        Programmable_thermostat pt;
        std::cout << pt.printable_time().c_str() << std::endl;
        pt.time_forward();
        std::cout << pt.printable_time().c_str() << std::endl;
        std::cout << pt.printable_target_temperature().c_str() << std::endl;
        
        try {
            pt.f_c();
        } catch (Invalid_temperature_switching_exception& itse) {
            std::cerr << itse.invalid_temperature_mode();
            std::exit(1);
        }
        std::cout << pt.printable_target_temperature().c_str() << std::endl;
    } catch (...) {
        std::abort();
    }

    return 0;
}

