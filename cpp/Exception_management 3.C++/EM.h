#ifndef _EM
#define _EM

#include <ctime>
#include <stdexcept>
#include <string>

class Invalid_temperature_exception : std::exception { // '#include <stdexcept>'
    float _value; // In Celsius
public:
    Invalid_temperature_exception(float);
    float value() const;
    const char* what() const throw (); // No exception may be thrown!
};

class Temperature {
public:

    enum Temperature_unit {
        Celsius, Fahrenheit, Kelvin
    };
    static const float Min;
private:
    float _value; // In Celsius
    float _step;
public:
    float asCelsius() const;
    float asFahrenheit() const;
    float asKelvin() const;
    void increment() throw ();
    void decrement() throw (float, std::bad_exception);
    int operator<(const Temperature&) const;
    int operator<=(const Temperature&) const;
    int operator>(const Temperature&) const;
    int operator>=(const Temperature&) const;
    Temperature();
    Temperature(float, enum Temperature_unit = Celsius) throw (char*, Invalid_temperature_exception/*, std::bad_exception*/);
};

class Global_exception_management {
public:
    static void my_unexpected();
    static void my_terminate();
};

class Invalid_temperature_switching_exception { // Caution: no inheritance here
private:
    Temperature::Temperature_unit _invalid_temperature_mode;
public:
    Invalid_temperature_switching_exception(Temperature::Temperature_unit);
    enum Temperature::Temperature_unit invalid_temperature_mode() const;
};

class Programmable_thermostat {
private:

    struct Program {
        Temperature target_temperature;
        std::time_t time;

        /** For test only: */
//        inline void f() {
//            if (_Max < _Min);
//        }
        /** End of test */
    };
public:
    static Temperature _Max;
    static Temperature _Min;
private:
protected:
    Program _program[8];
private:
    std::time_t _time;
    Temperature::Temperature_unit _temperature_mode;
    Temperature temperature() const;
    Temperature target_temperature;
public:
    Programmable_thermostat();
    void f_c() throw (Invalid_temperature_switching_exception);
    bool temp_down();
    bool temp_up();
    void time_backward();
    void time_forward();
    std::string printable_time() const;
    std::string printable_temperature() const;
    std::string printable_target_temperature() const;
};

#endif

