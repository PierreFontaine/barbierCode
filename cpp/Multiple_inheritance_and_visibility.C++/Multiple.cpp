#include <iostream>

#include "Multiple.h"

void A::f() {
    std::cout << "f from A" << '\n';
}

void B::f() {
    std::cout << "f from B" << '\n';
}

void C::f() {
    std::cout << "f from C" << '\n';
}

void D::g() {
    A* pA1;
    //    pA1 = this;
    pA1 = (B*)this;
//    pA1 = (C*)this;
    pA1->f();
}
