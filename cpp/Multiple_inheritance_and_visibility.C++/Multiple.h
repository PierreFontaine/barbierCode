#ifndef _Multiple_h
#define _Multiple_h

class A {
public:
    virtual void f();
};

class B : public A {
public:
    virtual void f();
};

class C : A {
public:
    virtual void f();
};

class D : B, C {
public:
    void g();
};

#endif
