#include <cassert>
#include <iostream>

#include "Hanoi.h"

Hanoi::Hanoi(int n) {
    assert(n > 0);
    for (int i = n; i > 0; i--) _from.push_back(i);
    hanoi(n, _from, _by, _to);
}

void Hanoi::hanoi(int n, list<int>& from, list<int>& by, list<int>& to) {
    if (!n) return;

    hanoi(n - 1, from, to, by);

    to.push_back(from.back());
    from.pop_back();

    list<int>::iterator i;
    std::cout << std::endl << "*** New recursion step: ***";
    std::cout << std::endl << "from :";
    for (i = from.begin(); i != from.end(); i++) std::cout << '\t' << *i;
    std::cout << std::endl;
    std::cout << std::endl << "by :";
    for (i = by.begin(); i != by.end(); i++) std::cout << '\t' << *i;
    std::cout << std::endl;
    std::cout << std::endl << "to :";
    for (i = to.begin(); i != to.end(); i++) std::cout << '\t' << *i;
    std::cout << std::endl;

    hanoi(n - 1, by, from, to);
}

int main(int argc, char** argv) {
    Hanoi h(3);

    return 0;
}

