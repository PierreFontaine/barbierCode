#ifndef _Hanoi
#define _Hanoi

#include <list>

using namespace std;

class Hanoi {
private:
    list<int> _from;
    list<int> _by;
    list<int> _to;
    void hanoi(int, list<int>&, list<int>&, list<int>&);
public:
    Hanoi(int);
};

#endif
