#include "Message.h"

const unsigned int Message::Capacity = Bit_number * 2;

unsigned int Message::minindex() {
    return Unlimited_bit_stream::minindex();
}

Message::Message(const Limited_bit_stream& x) {
    if (x.size() != Message::Capacity || x[0]) this->Unlimited_bit_stream::operator=(Unlimited_bit_stream("0000000000000000"));
    else this->Unlimited_bit_stream::operator=(Unlimited_bit_stream(x));
}

Message::Message(const Unlimited_bit_stream& x) {
    if (x.size() != Message::Capacity || x[0]) this->Unlimited_bit_stream::operator=(Unlimited_bit_stream("0000000000000000"));
    else this->Unlimited_bit_stream::operator=(x);
}

Message::Message(const Message& x) {
    *this = x;
}

unsigned int Message::size() const {
    return Message::Capacity;
}

Limited_bit_stream::Bit Message::start_bit() const {
    return operator[](0);
}

Limited_bit_stream Message::address() const {
    char buffer[] = "0000000000\0";
    for (int i = 1; i <= 10; i++) {
        if (operator[](i)) buffer[i - 1] = '1';
    }
    return Limited_bit_stream(buffer);
}

Limited_bit_stream::Bit Message::direction() const {
    return operator[](11);
}

Limited_bit_stream Message::command_code() const {
    char buffer[] = "000\0";
    for (int i = 12; i <= 14; i++) {
        if (operator[](i)) buffer[i - 12] = '1';
    }
    return Limited_bit_stream(buffer);
}

Limited_bit_stream::Bit Message::parity() const {
    return operator[](15);
}

const Message& Message::operator=(const Message& x) {
    if (this != &x) this->Unlimited_bit_stream::operator=(x);
    return *this;
}

Message Message::operator+(const Message&) const {
    return *this;
}

Message& Message::operator+=(const Message&) {
    return *this;
}

