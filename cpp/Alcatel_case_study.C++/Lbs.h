#ifndef _Lbs_h
#define _Lbs_h

#define Bit_number 8

class Limited_bit_stream {
private:
    typedef unsigned long Bit_stream_format;

public:
    typedef short Bit;

public:
    static const unsigned int Capacity;
    static unsigned int minindex();

private:
    Bit_stream_format _buffer;
    unsigned int _length;

public:
    Limited_bit_stream();
    Limited_bit_stream(const char[]);
    Limited_bit_stream(Bit_stream_format);

    char* format(char[]) const; // C style
    unsigned int size() const;
    Bit operator[](unsigned int) const; // Get bit
    // Bit& operator [] (unsigned int); // Get + set bit
    void operator()(unsigned int, Bit); // Set bit
    Limited_bit_stream operator+(const Limited_bit_stream&) const;
    Limited_bit_stream& operator+=(const Limited_bit_stream&);
};

#endif
