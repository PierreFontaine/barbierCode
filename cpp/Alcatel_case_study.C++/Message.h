#ifndef _Message_h
#define _Message_h

#include "Ubs.h"

class Message : public Unlimited_bit_stream {
public:
    static const unsigned int Capacity;
    static unsigned int minindex();

public:
    Message(const Limited_bit_stream&);
    Message(const Unlimited_bit_stream&);
    Message(const Message&);

    unsigned int size() const;
    Message operator+(const Message&) const;
    Message& operator+=(const Message&);
    const Message& operator=(const Message&);
    Limited_bit_stream::Bit start_bit() const; // Bit 1 is set to 0
    Limited_bit_stream address() const; // Bits 2->11
    Limited_bit_stream::Bit direction() const; // Bit 12
    Limited_bit_stream command_code() const; // Bits 13->15
    Limited_bit_stream::Bit parity() const; // Bit 16 (even)
};

#endif
