#include <cassert>
#include <cstring>

#include "Lbs.h"

// Old matter:
//extern "C" { // This declaration style prevents the activation of the "decoration algorithm" to C functions
//    char* std::strcat(char*, const char*);
//    size_t std::strlen(const char*);
//}

const unsigned int Limited_bit_stream::Capacity = sizeof (Bit_stream_format) * Bit_number;

unsigned int Limited_bit_stream::minindex() {
    return 0;
}

Limited_bit_stream::Limited_bit_stream() {
    _buffer = 0L;
    _length = 0;
}

Limited_bit_stream::Limited_bit_stream(const char a_string[]) {
    _buffer = 0L;
    _length = ::std::strlen(a_string); // 'std::strlen' counts the number of characters before the "end" character, i.e., '\0'
    if (_length <= Capacity) {
        unsigned int index;
        for (index = 0; index < _length; index++) {
            if (a_string[index] == '1') {
                _buffer <<= 1; // 1-bit left shift de 1 bit and re-assignment
                _buffer |= 1L; // Logical "OR" with 0x0001 and re-assignment
            } else {
                if (a_string[index] == '0') _buffer <<= 1;
                else {
                    _buffer = 0L;
                    _length = 0;
                    break;
                }
            }
        }
    } else {
        _buffer = 0L;
        _length = 0;
    }
}

Limited_bit_stream::Limited_bit_stream(Bit_stream_format a_Bit_stream_format) {
    _buffer = a_Bit_stream_format;
    _length = Capacity;
}

char* Limited_bit_stream::format(char a_string[]) const {
    if (!_length) return a_string = ""; // Incorrect in modern C++
    unsigned int index;
    for (index = 0; index < _length; index++) {
        if ((signed long) (_buffer << (index + Capacity - _length)) < 0) // '(signed long)' has greater priority over '<<', which has greater priority over '<' -> parentheses required
            a_string[index] = '1';
        else
            a_string[index] = '0';
    }
    a_string[_length] = '\0';
    return a_string;
}

unsigned int Limited_bit_stream::size() const {
    return _length;
}

Limited_bit_stream::Bit Limited_bit_stream::operator[](unsigned int index) const {
    assert(index < _length);
    if (_buffer >> index & 1) return 1; // '>>' is a right shift since '_buffer' is 'unsigned'
    else return 0;
}

void Limited_bit_stream::operator()(unsigned int index, Bit a_bit) {
    assert(index < _length && a_bit == 0 || a_bit == 1);
    Bit_stream_format bsf = 1L;
    bsf <<= index;
    if (a_bit == 0) {
        bsf = ~bsf; // Complement to 1
        _buffer &= bsf;
    } else {
        if (a_bit == 1) {
            _buffer |= bsf;
        }
    }
}

Limited_bit_stream Limited_bit_stream::operator+(const Limited_bit_stream& a_Limited_bit_stream) const {
    if (_length + a_Limited_bit_stream._length > Capacity) return Limited_bit_stream();
    else {
        char* buffer1 = new char[_length + a_Limited_bit_stream._length + 1];
        char* buffer2 = new char[a_Limited_bit_stream._length + 1];
        Limited_bit_stream lbs(::std::strcat(format(buffer1),
                a_Limited_bit_stream.format(buffer2)));
        delete[] buffer2;
        delete[] buffer1;
        return lbs;
    }
}

Limited_bit_stream& Limited_bit_stream::operator+=(const Limited_bit_stream& a_Limited_bit_stream) {
    if (_length + a_Limited_bit_stream._length > Capacity) return *this;
    else {
        *this = *this +a_Limited_bit_stream;
        return *this;
    }
}
