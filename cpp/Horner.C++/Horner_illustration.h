#ifndef _Horner_illustration_h
#define _Horner_illustration_h

class Horner_illustration { // Polynomial evaluation according to different methods
    int _size;
    double* _representation;
public:
    Horner_illustration(int);
    ~Horner_illustration();
    double eval1(double);
    double eval2(double);
    double eval3(double);
    double horner(double);
};

#endif
