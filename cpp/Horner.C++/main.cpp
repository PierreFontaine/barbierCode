#include <math.h>
#include <stdlib.h>
#include <time.h>

#include <cassert>

#include "Horner_illustration.h"

extern "C" {
    double pow(double, double); // math.h
    int rand(); // stdlib.h
    void srand(unsigned int); // stdlib.h
    time_t time(time_t*); // time.h
}

Horner_illustration::Horner_illustration(int size) : _size(size) {
    assert(_size > 0);
    _representation = new double[_size];
    time_t t;
    ::srand(::time(&t));
    for (int i = 0; i < _size; i++) _representation[i] = (double) ::rand();
}

Horner_illustration::~Horner_illustration() {
    delete[] _representation;
}

double Horner_illustration::eval1(double x) {
    double result = 0;
    for (int i = 0; i < _size; i++) result += (_representation[i] * ::pow(x, i));
    return result;
}

double Horner_illustration::eval2(double x) {
    double result = 0;
    for (int i = 1; i < _size; i++) {
        double xi = 1;
        for (int j = 1; j <= i; j++) xi = xi * x;
        result = result + (_representation[i] * xi);
    }
    return result + _representation[0];
}

double Horner_illustration::eval3(double x) {
    double result = 0;
    double xi = 1;
    for (int i = 1; i < _size; i++) {
        xi *= x;
        result = result + (_representation[i] * xi);
    }
    return result + _representation[0];
}

double Horner_illustration::horner(double x) {
    double result = 0;
    for (int i = _size - 1; i > 0; i--) result = (result + _representation[i]) * x;
    return result + _representation[0];
}

int main(int argc, char** argv) {
    Horner_illustration hi(5);
    time_t t;
    ::srand(::time(&t));
    hi.horner(double(::rand()));

    return 0;
}

