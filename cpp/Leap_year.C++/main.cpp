#include <ctime>
#include <iostream>

int main(int argc, char** argv) {

    std::time_t now = std::time(&now); // '#include <ctime>'
    std::tm *now_as_data_structure = std::gmtime(&now);
    int year = now_as_data_structure->tm_year + 1900; // Caution: 'tm_year' returns the current year minus '1900'!

    if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)) std::cout << std::asctime(std::localtime(&now)) << " is a leap year..." << std::endl;
    else std::cout << std::asctime(std::localtime(&now)) << " is NOT a leap year..." << std::endl;

    return 0;
}

