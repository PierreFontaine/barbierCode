#include <iostream>

#include "Covariance.h"

const Human_being* Human_being::cloning() const {
    std::cout << "'cloning()' in 'Human_being' is called...\n";
    return new Human_being; // Bad programming because 'delete' must be later called in calling context
}

const Man* Man::cloning() const {
    std::cout << "'cloning()' in 'Man' is called...\n";
    return new Man; // Bad programming because 'delete' must be later called in calling context
}

const Woman* Woman::cloning() const {
    std::cout << "'cloning()' in 'Woman' is called...\n";
    return new Woman; // Bad programming because 'delete' must be later called in calling context
}

int main(int argc, char** argv) {
    const Human_being * const FranckBarbier = new Man;
    const Human_being* clone = FranckBarbier->cloning(); // 'cloning()' in 'Man' is called... => covariance, yes!
    delete clone;
    delete FranckBarbier;
    return 0;
}

