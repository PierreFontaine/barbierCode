#ifndef _Covariance_H
#define	_Covariance_H

#include <iostream>

class Human_being {
public:
    virtual const Human_being* cloning() const;

    virtual ~Human_being() {
        std::cout << "Human_being";
    }
};

class Man : public Human_being {
public:
    const Man* cloning() const;

    ~Man() {
        std::cout << "-Man";
    }
};

class Woman : public Human_being {
public:
    const Woman* cloning() const;

    ~Woman() {
        std::cout << "-Woman";
    }
};

#endif	/* _Covariance_H */

