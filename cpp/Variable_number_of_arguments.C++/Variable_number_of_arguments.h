#ifndef _Variable_number_of_arguments_H
#define	_Variable_number_of_arguments_H

#include <time.h>

#include <set>

class Human_being {
private:
    tm _birth_date;
public:
    Human_being(int = 01, int = 01, int = 1970);
    int age() const;
    const Human_being cloning() const;
private:
    std::set<Human_being*> _children;
public:
    void births(Human_being*...);
    void births(int, Human_being*...);
};

class Man : public Human_being {
public:
    const Man cloning() const;
};

class Woman : public Human_being {
public:
    const Woman cloning() const;
};

#endif	/* _Variable_number_of_arguments_H */

