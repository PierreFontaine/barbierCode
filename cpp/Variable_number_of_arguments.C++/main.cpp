#include <cstdarg> // 'va_list' type
#include <cstdlib> // 'NULL'
#include <iostream>

#include "Variable_number_of_arguments.h"

using namespace std;

Human_being::Human_being(int day_of_month, int month, int year) {
    _birth_date.tm_mday = day_of_month;
    _birth_date.tm_mon = month;
    _birth_date.tm_year = year;
}

int Human_being::age() const {
    time_t now = ::time(&now);
    return ::gmtime(&now)->tm_year + 1900 - _birth_date.tm_year;
}

const Human_being Human_being::cloning() const {
    std::cout << "'cloning()' in 'Human_being' is called...\n";
    return *this;
}

void Human_being::births(Human_being* child...) {
    va_list children;
    va_start(children, child);
    for (Human_being* e = child; e != NULL; e = va_arg(children, Human_being*)) _children.insert(e);
    va_end(children);
}

void Human_being::births(int nb, Human_being* child...) {
    va_list children;
    va_start(children, child);
    Human_being* e = child;
    for (int i = 0; i < nb; i++, e = va_arg(children, Human_being*)) _children.insert(e);
    va_end(children);
}

const Man Man::cloning() const {
    std::cout << "'cloning()' in 'Man' is called...\n";
    return *this;
}

const Woman Woman::cloning() const {
    std::cout << "'cloning()' in 'Woman' is called...\n";
    return *this;
}

int main(int argc, char** argv) {
    Human_being FranckBarbier(11, 01, 1963);
    int age = FranckBarbier.age();

    Human_being* man = new Man();
    Human_being clone = man->cloning(); // 'cloning()' in 'Human_being' is called...
    delete man;

    FranckBarbier.births(new Human_being(6, 12, 1993), new Human_being(15, 4, 1996), new Human_being(22, 2, 2001), NULL);
    FranckBarbier.births(3, new Human_being(6, 12, 1993), new Human_being(15, 4, 1996), new Human_being(22, 2, 2001));
    return 0;
}

