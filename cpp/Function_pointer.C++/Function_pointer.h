#ifndef _Function_pointer_h
#define _Function_pointer_h 

#include <ctime>

class Meal {
public:
    static const std::tm Defaut_moment;

    enum Type {
        Breakfast, Lunch, Afternoon_tea, Dinner
    };
private:
    enum Type _type;
    std::tm _moment;
public:
    Meal(enum Type = Breakfast, const std::tm& = Defaut_moment);
    const std::tm& moment() const;
};

#endif          
