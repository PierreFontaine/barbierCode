#include <iostream>

#include "Function_pointer.h"

const std::tm Meal::Defaut_moment = {0, 0, 12, 15, 3, 96}; // April 15, 1996

Meal::Meal(enum Type type, const std::tm& moment) : _type(type), _moment(moment) {
}

const std::tm& Meal::moment() const {
    return _moment;
}

int main(int argc, char** argv) {
    time_t clock = std::time(&clock);
    std::tm* now = std::gmtime(&clock);

    Meal m1(Meal::Dinner, *now);
    std::cout << "m1 at " << std::asctime(&(m1.moment())) << "\n\n";

    Meal m2;
    std::cout << "m2 at " << std::asctime(&(m2.moment())) << "\n\n";

    const std::tm & (Meal::*function_pointer)() const; // Pointer declaration
    function_pointer = &Meal::moment; // Pointer assignment
    const std::tm moment = (m1.*function_pointer)(); // Pointer use
    std::cout << "m1 at " << std::asctime(&(moment)) << "\n\n";

    char* (*pointer_asctime)(const std::tm*); // Pointer declaration
    pointer_asctime = &std::asctime; // Pointer assignment
    std::cout << "m2 at " << (*pointer_asctime)(&(m2.moment())) << "\n\n"; // Pointer use

    return 0;
}

