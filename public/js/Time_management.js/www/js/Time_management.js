/*
 * Time_management.js
 */

'use strict';

function main() {
// All resources are available including downloaded images...
//    document.body.innerWidth = 800;
//    document.body.innerHeight = 600;
    new Time_management('img/Bus.jpg', 20, 5); // Each 4 sec. within 20 sec.
}

var Time_management = function (image_name, count_down, steps) {
    this._image_name = image_name;
    this._animation_id = null;
    this._interval_id = null;

    this._count_down = count_down;
    this._steps = steps;
    this._time_left = count_down; // In sec..
    /**
     Event handling policy
     */
    document.addEventListener('Image_is_ready', this._create_3D_object.bind(this, this._image_name), false);
    document.addEventListener('3D_object_is_ready', function () { // 'custom_event' is ignored
        this._interval_id = window.setInterval(this._time_out.bind(this), count_down / steps * 1000);
    }.bind(this), false);
    document.addEventListener('3D_object_is_ready', function (custom_event) {
        if (custom_event.detail.name !== this._image_name)
            throw("Abnormal situation...");
        this._animation_id = window.requestAnimationFrame(this._animate_3D_object.bind(this));
    }.bind(this), false);

    this._image = new Image();
    this._image.onload = function () {
        document.dispatchEvent(new Event('Image_is_ready'));
    };
    this._image.src = image_name;

    this._camera = new THREE.PerspectiveCamera(50, window.innerWidth / window.innerHeight, 0.1, 5000);
    this._camera.position.x += 100;
    this._camera.position.y += 100;
    this._camera.position.z = 3000;

    // Using 'THREE.CanvasRenderer' decreases performance:
    if (Detector.webgl) {
        this._renderer = new THREE.WebGLRenderer({/*alpha: true,*/ antialias: true});
        this._renderer.setPixelRatio((window.devicePixelRatio) ? window.devicePixelRatio : 1); // If exists then OK else '1'
    } else {
        this._renderer = new THREE.CanvasRenderer({overdraw: true});
    }
    this._renderer.setClearColor(new THREE.Color("rgb(211,211,211)"), 0.5); // Silver
    this._renderer.setSize(window.innerWidth, window.innerHeight);
    document.body.appendChild(this._renderer.domElement);
    this._scene = new THREE.Scene();
    this.buildAxes(1000);
};

Time_management.prototype._animate_3D_object = function (timestamp) {
    if (timestamp !== undefined) { /* first call: 'timestamp === undefined' */
        // One may handle the elapsed time from the first call to '_animate_3D_object'
    }
    this._scene.getObjectByName(this._image_name).rotation.y -= 0.01;
    this._scene.getObjectByName(this._image_name).material.opacity -= 0.0008;
    this._renderer.render(this._scene, this._camera);
    this._animation_id = window.requestAnimationFrame(this._animate_3D_object.bind(this));
};

Time_management.prototype._create_3D_object = function (name) {
    if (!(this._image !== undefined && this._image !== null && this._image instanceof Image && this._image.complete))
        throw("Abnormal situation...");

    var canvas = document.createElement('canvas');
    canvas.width = this._image.width;
    canvas.height = this._image.height;
    canvas.getContext('2d').drawImage(this._image, 0, 0, this._image.width, this._image.height);

    var texture = new THREE.CanvasTexture(canvas);
    texture.minFilter = THREE.LinearFilter;

    var mesh = new THREE.Mesh(new THREE.SphereGeometry(600, 0, 0, 0, Math.PI), new THREE.MeshBasicMaterial({
        map: texture,
        opacity: 1.,
        side: THREE.DoubleSide,
        transparent: true,
        vertexColors: THREE.FaceColors,
    }));
    mesh.name = name;
    this._scene.add(mesh);

    document.dispatchEvent(new CustomEvent('3D_object_is_ready', {'detail': {name: name}}));
};

Time_management.prototype._time_out = function () {
    if ((this._time_left -= this._count_down / this._steps) > 0)
        swal({
            showConfirmButton: false,
            title: 'You may miss the bus!',
            text: this._time_left + ' sec.',
            timer: 1000
        });
    else {
        window.clearInterval(this._interval_id);

        this._scene.getObjectByName(this._image_name).material.opacity = 1;
        this._scene.getObjectByName(this._image_name).material.color = new THREE.Color("rgb(255,0,0)");
        this._renderer.render(this._scene, this._camera);
        window.cancelAnimationFrame(this._animation_id);

        swal({
            showConfirmButton: false,
            title: 'Oops...',
            text: 'You missed it!',
            type: 'warning'
        });
    }
};

Time_management.prototype.buildAxes = function (length) {
    var axes = new THREE.Object3D();
    axes.add(this.buildAxis(new THREE.Vector3(0, 0, 0), new THREE.Vector3(length, 0, 0), new THREE.Color('red'), false)); // +X
    axes.add(this.buildAxis(new THREE.Vector3(0, 0, 0), new THREE.Vector3(-length, 0, 0), new THREE.Color('red'), true)); // -X
    axes.add(this.buildAxis(new THREE.Vector3(0, 0, 0), new THREE.Vector3(0, length, 0), new THREE.Color('blue'), false)); // +Y
    axes.add(this.buildAxis(new THREE.Vector3(0, 0, 0), new THREE.Vector3(0, -length, 0), new THREE.Color('blue'), true)); // -Y
    axes.add(this.buildAxis(new THREE.Vector3(0, 0, 0), new THREE.Vector3(0, 0, length), new THREE.Color('green'), false)); // +Z
    axes.add(this.buildAxis(new THREE.Vector3(0, 0, 0), new THREE.Vector3(0, 0, -length), new THREE.Color('green'), true)); // -Z
    this._scene.add(axes);
};

Time_management.prototype.buildAxis = function (src, dst, color, is_dashed) {
    var geometry = new THREE.Geometry();
    geometry.vertices.push(dst);
    geometry.vertices.push(src);
    geometry.computeLineDistances(); // This one is very important, otherwise dashed lines will appear as simple plain lines
    return new THREE.LineSegments(geometry, is_dashed ? new THREE.LineDashedMaterial({linewidth: 5, color: color.getHex(), dashSize: 5, gapSize: 10}) : new THREE.LineBasicMaterial({linewidth: 5, color: color.getHex()}));
};



