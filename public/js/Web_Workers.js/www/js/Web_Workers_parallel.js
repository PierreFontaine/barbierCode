/*
 * Web_Workers_parallel.js
 */

"use strict";

self.importScripts("chai.js"); // Reused libraries require local-scope load...

function process(image_data) {
    var buffer = new Uint32Array(image_data.data.buffer);
    for (var pixel_number = 0; pixel_number < buffer.length; pixel_number++) {
        var alpha = buffer[pixel_number] >>> 24; // Most left byte
        var blue = (buffer[pixel_number] & 0x00FF0000) >> 16;
        var green = (buffer[pixel_number] & 0x0000FF00) >> 8;
        var red = buffer[pixel_number] & 0x000000FF; // Most right byte
// Simplistic image processing:
        blue = ~blue;
        green = ~green;
        // red = ~red;
        buffer[pixel_number] = (alpha << 24) | (blue << 16) | (green << 8) | red;
    }
}

onmessage = function (message) { // Message from 'main' thread, i.e., 'worker.postMessage($canvas.getContext('2d').getImageData(0, 0, $canvas.width, $canvas.height));', is received...
    chai.assert.isTrue(message.data instanceof ImageData); // Check received data
    process(message.data);
    postMessage(message.data); // Response from worker to 'main' thread...
};
