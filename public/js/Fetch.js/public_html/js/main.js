"use strict";

let _DOM_ready;
let DOM_ready = new Promise((function_launched_when_DOM_ready) => {
    _DOM_ready = function_launched_when_DOM_ready;
}).then(value => {
    let image_tag = document.createElement('img');
    image_tag.setAttribute('id', 'my_image_tag');
    image_tag.setAttribute('align', 'middle');
    document.body.appendChild(image_tag);
    return image_tag;
});
document.onreadystatechange = _DOM_ready;

// 'fetch' support at 'http://caniuse.com/#feat=fetch'
Promise.all([Promise.resolve(window.fetch !== undefined), DOM_ready, fetch("img/Large_image.jpg").then(function (response) {
        return response.blob();
    })]).then(parameters => {
    if (parameters[0] === true) // 'fetch' is supported by the browser
//    'parameters[1]' -> DOM is ready with access to 'image_tag'
//    'parameters[2]' -> Image is available (i.e., 'complete') and transformed into blob
        parameters[1].src = URL.createObjectURL(parameters[2]); // 'image_tag' is loaded from "img/Large_image.jpg"
});




