/*
 * jQuery_main.js
 */

"use strict";

function main() {

    $(document).on("Image_just_processed", function_in_charge_of_event_processing); // Event processing policy
    function function_in_charge_of_event_processing($event, parameters) { // Event processing
        chai.assert.strictEqual($event.type, "Image_just_processed"); // 'chai.js' nice third-party library for contract-based programming
        swal(parameters.data1 + " just loaded in canvas with width: " + parameters.data2 + " and height: " + parameters.data3); // 'sweetalert.js nice library'
    }

    var $canvas = $("<canvas>").attr({
        id: "My canvas"
    }).css({
        position: "absolute",
        border: "5px solid red"
    });
    $("body").append($canvas);

    var image = new Image();
    image.onload = function () {
        $canvas.get(0).width = image.width;
        $canvas.get(0).height = image.height;
        $canvas.get(0).getContext('2d').drawImage(image, 0, 0); // Image is loaded in canvas
        $(document).trigger("Image_just_processed", {data1: image.src, data2: image.width, data3: image.height});
    };
    image.src = "img/Franck.jpg"; // Image load...
}
