/*
 * Web_Services_main.js
 */

"use strict";
function main() {
    // Calling Web services
    var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        console.log(request.getAllResponseHeaders());
        if (request.readyState === 4) {
            if (request.getResponseHeader("Content-Type").includes("application/json")) {
                var response = JSON.parse(request.responseText);
                alert("For 1 US Dollar (USD) to MAD (Moroccan Dirham): " + response.rates.MAD);
            }
        }
    };
    request.open("GET", "http://openexchangerates.org/api/latest.json" + "?app_id=" + "678cd96edd4b4f3eb637bf74ef8e0815", true); // This Web site requires a license key after signing in
// 'request' must already be opened:
    request.send(null);

//    var request2 = new XMLHttpRequest(); // Stuck by CORS
//    request2.onreadystatechange = function () {
//        if (request2.readyState === 4) {
//            alert(request2.responseText);
//        }
//    };
//    request2.open("POST", "http://www.webservicex.net/ConvertTemperature.asmx/ConvertTemp", true);
//    request2.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); // As a HTML form
//    request2.send("Temperature=18&FromUnit=degreeCelsius&ToUnit=degreeFahrenheit");

    var request3 = new XMLHttpRequest(); // Stuck by CORS
    request3.onreadystatechange = function () {
        if (request3.readyState === 4) {
            // Process response as XML text...
        }
    };
    request3.open("POST", "http://www.webservicex.net/ConvertTemperature.asmx/ConvertTemp", true);
    request3.setRequestHeader("Content-Type", "application/soap+xml");
    var content =
            "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
            "<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">" +
            "  <soap12:Body>" +
            "   <ConvertTemp xmlns=\"http://www.webservicex.net/\">" +
            "     <Temperature>18</Temperature>" +
            "     <FromUnit>degreeCelsius</FromUnit>" +
            "     <ToUnit>degreeFahrenheit</ToUnit>" +
            "   </ConvertTemp>" +
            " </soap12:Body>" +
            "</soap12:Envelope>";
    request3.send(content);
}
