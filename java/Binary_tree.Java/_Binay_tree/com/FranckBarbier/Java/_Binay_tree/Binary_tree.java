package com.FranckBarbier.Java._Binay_tree;

public class Binary_tree<T> {

    protected T _content;
    protected Binary_tree<T> _left, _right, _super;

    public void infix() {
        if (_left != null) {
            _left.infix();
        }
        System.out.println();
        if (_right != null) {
            _right.infix();
        }
    } // a+b-c*d

    public void prefix() {
        System.out.println();
        if (_left != null) {
            _left.prefix();
        }
        if (_right != null) {
            _right.prefix();
        }
    } // +a*-bcd

    public void postfix() {
        if (_left != null) {
            _left.postfix();
        }
        if (_right != null) {
            _right.postfix();
        }
        System.out.println();
    } // abc-d*+

    public int h() {
        if (_super == null) {
            return 0;
        }
        return _super.h() + 1;
    }

    public int H() {
        int H_left, H_right;
        if (_left != null) {
            H_left = _left.H() + 1;
        } else {
            H_left = 0;
        }
        if (_right != null) {
            H_right = _right.H() + 1;
        } else {
            H_right = 0;
        }
        return H_left < H_right ? H_right : H_left;
    }

    public boolean isAVL() {
        if (_left == null && _right == null) {
            return true;
        }
        if (_left == null) {
            return _right.H() < 1;
        }
        if (_right == null) {
            return _left.H() < 1;
        }
        return false;
    }

    public static void main(String[] args) {
        Binary_tree<Character> bt;

        java.util.Vector<Integer> vector = new java.util.Vector<Integer>();
        vector.addElement(11);
        vector.addElement(28);
        vector.addElement(6);
        vector.addElement(15);
        vector.addElement(22);
        java.util.Collections.sort(vector);
        assert (java.util.Collections.binarySearch(vector, 13) >= 0);
        assert (java.util.Collections.binarySearch(vector, 14) < 0);
    }
}
