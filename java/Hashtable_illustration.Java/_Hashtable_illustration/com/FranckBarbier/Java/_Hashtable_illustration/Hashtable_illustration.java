package com.FranckBarbier.Java._Hashtable_illustration;

public class Hashtable_illustration {

    public static void main(String args[]) {
        String s1 = "Franck";
        String s2 = "Franck";
        assert (s1 != s2); // 's1' and 's2' are NOT the same memory object
        assert (s1.equals(s2)); // However, 's1' equals to 's2'
        assert (s1.hashCode() == s2.hashCode());

        java.util.Map<String, String> given_names_dictionary = new java.util.Hashtable<String, String>(5, .5F);
        given_names_dictionary.put("Franck", "German origin...");
        given_names_dictionary.put("Frank", "English origin...");
        assert (given_names_dictionary.size() == 2);

        java.util.Map<Object, String> hashtable_with_possible_null_keys = new java.util.HashMap<Object, String>();
        hashtable_with_possible_null_keys.put(null, "Allowing 'null' keys");
        System.out.println(hashtable_with_possible_null_keys.get(null));
    }
}
