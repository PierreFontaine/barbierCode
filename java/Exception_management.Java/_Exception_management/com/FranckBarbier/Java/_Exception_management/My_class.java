package com.FranckBarbier.Java._Exception_management;

import com.FranckBarbier.Java._Temperature.Invalid_temperature_exception;
import com.FranckBarbier.Java._Temperature.Temperature;

public class My_class implements Cloneable {

    void f() throws Invalid_temperature_exception {
        Temperature t = new Temperature();
        try {
            while (true) {
                t.decrement();
            }
        } //        catch(AssertionError ae) {
        //            traitement d�ae
        //        }
        catch (Invalid_temperature_exception ite) {
            System.err.println("Temperature raising problem: " + t.toString(Temperature.Temperature_unit.Celsius));
            throw ite;
        } finally {
            System.err.println("Message is appearing, exception ot not!");
        }
    }
    public final int i = 2008;
//    public Object clone() throws CloneNotSupportedException {
//        return super.clone();
//    }
//    public Object clone() {
//        return new My_class();
//    }

    public static void main(String[] args) {
        My_class c1 = new My_class(), c2 = null;
//        c2 = (My_class)c1.clone();
        try {
            c2 = (My_class) c1.clone();
        } catch (RuntimeException re) {
            re.printStackTrace();
        } catch (CloneNotSupportedException cnse) {
            cnse.printStackTrace();
        }
        if (c1 instanceof Object) {
            System.out.println("OK");
        }
    }
}
