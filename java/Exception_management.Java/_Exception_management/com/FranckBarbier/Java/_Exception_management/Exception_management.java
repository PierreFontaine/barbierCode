package com.FranckBarbier.Java._Exception_management;

import com.FranckBarbier.Java._Temperature.Invalid_temperature_exception;
import com.FranckBarbier.Java._Temperature.Temperature;

public class Exception_management {

    public static void main(String[] args) {
        try {
            Temperature t = new Temperature(0.F, Temperature.Temperature_unit.Kelvin);
            t.decrement();
        } catch (Invalid_temperature_exception ite) {
            // System.err.println("Temperature causing problem: " + t.toString(Temperature.Temperature_unit.Celsius));
        } catch (Exception e) {

        } catch (Throwable t) {
            System.err.println(t.getMessage());
        } finally {
            System.err.println("Message is appearing, exception or not!");
        }

//        Temperature t = new Temperature();
//        try {
//            while(true) t.decrement();
//        } catch(Invalid_temperature_exception ite) {
//            System.err.println("Temperature causing problem: " + t.toString(Temperature.Temperature_unit.Celsius));
//        } finally {
//            System.err.println("Message appears, with or without exception...");
//        }
        /*
         try {
         // Any Java statement
         } catch(Throwable x) {
         if(x instanceof Invalid_temperature_exception);
         }
         */
    }
}
