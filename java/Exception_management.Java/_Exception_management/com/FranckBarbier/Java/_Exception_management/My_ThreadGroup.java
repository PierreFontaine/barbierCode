package com.FranckBarbier.Java._Exception_management;

class My_ThreadGroup extends ThreadGroup {

    public My_ThreadGroup(String name) {
        super(name);
    }

    public void uncaughtException(Thread thread, Throwable exception) {
        System.err.println("Uncaught exception: " + exception);
    }

    public static void main(String[] args) {
//        Thread thread = new Thread(new My_ThreadGroup("Thread group name"), "Thread name");
//        thread.start();
        Thread thread = new Thread("Thread name");
        thread.setUncaughtExceptionHandler(new My_UncaughtExceptionHandler());
        thread.start();
        Thread.currentThread().setUncaughtExceptionHandler(new My_UncaughtExceptionHandler());
    }
}
