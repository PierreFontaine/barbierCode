package com.FranckBarbier.Java._Covariance;

public class Family {

    java.util.Set<Human_being> _members = new java.util.HashSet<Human_being>();

    public Family() {
        _members.add(new Human_being(11, 1, 1963));
        _members.add(new Human_being(28, 7, 1964));
    }

    public void births(Human_being... children) {
        for (Human_being c : children) {
            _members.add(c);
        }
    }

    public int last_child_birth_year() {
        int result = 0;
        for (Human_being membre : _members) {
            if (result < membre._birth_date.get(java.util.Calendar.YEAR)) {
                result = membre._birth_date.get(java.util.Calendar.YEAR);
            }
        }
        return result;
    }
}
