package com.FranckBarbier.Java._Polymorphism;

public class Note {

    private static int _Increment = 10;
    private final java.io.StringReader _source;
    public final static String Default_header = "Note: ";
    protected StringBuffer _text;

    private void initialization() {
        _text = new StringBuffer(Default_header);
    }

    public Note(java.io.StringReader source) {
        _source = source;
        initialization();
    }

    private void collect_data() throws java.io.IOException {
        int end_of_source = 0;
        while (_source.ready() && end_of_source != -1) {
            if (_text.capacity() <= _text.length()) {
                _text.ensureCapacity(_text.length() + _Increment);
            }
            end_of_source = _source.read();
            if (end_of_source != -1) {
                _text.append((char) end_of_source);
            }
        }
    }

    public String write() throws java.io.IOException {
        collect_data();
        _text.trimToSize();
        return _text.toString();
    }
}
