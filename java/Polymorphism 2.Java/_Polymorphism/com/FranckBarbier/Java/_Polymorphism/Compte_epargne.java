package com.FranckBarbier.Java._Polymorphism;

abstract public class Compte_epargne extends Compte_bancaire {

    protected Compte_epargne(int id, float solde) {
        super(id, solde);
    }
}
