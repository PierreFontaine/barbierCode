package com.FranckBarbier.Java._Polymorphism;

public class Livret_A extends Compte_epargne {

    public final static float _taux_interet = 3.F;

    public Livret_A(int id, float solde) {
        super(id, solde);
    }

    public float taux_interet() {
        return _taux_interet;
    }
}
