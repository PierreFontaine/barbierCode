package com.FranckBarbier.Java._Polymorphism;

public class Compte_cheque extends Compte_bancaire {

    protected float _taux_interet;
    protected float _seuil;

    public Compte_cheque(int id, float solde, float taux_interet, float seuil) {
        super(id, solde); // avant toute autre instruction dans le constructeur
        _taux_interet = taux_interet;
        _seuil = seuil;
        assert (_taux_interet >= 0.F && _taux_interet < 100.F);
    }

    public float taux_interet() {
        return _taux_interet;
    }

    @Override
    public void appliquer_taux_interet() {
        if (_solde > _seuil) {
            super.appliquer_taux_interet();
        }
    }
}
