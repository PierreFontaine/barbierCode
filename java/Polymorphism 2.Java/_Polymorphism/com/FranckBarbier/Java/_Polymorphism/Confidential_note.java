package com.FranckBarbier.Java._Polymorphism;

public class Confidential_note extends Note {

    public final static String Default_header = "Confidential note: ";

    private void initialization() {
        _text = new StringBuffer(Default_header);
    }

    public Confidential_note(java.io.StringReader source) {
        super(source);
    }

    @Override
    public String write() throws java.io.IOException {
        return super.write() + " {Nobody should read it...}";
    }
}
