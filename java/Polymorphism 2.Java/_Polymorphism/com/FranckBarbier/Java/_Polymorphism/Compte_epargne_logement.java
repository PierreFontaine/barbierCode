package com.FranckBarbier.Java._Polymorphism;

public class Compte_epargne_logement extends Compte_epargne {

    public final static float _taux_interet = Livret_A._taux_interet * 2.F / 3.F;

    public Compte_epargne_logement(int id, float solde) {
        super(id, solde);
    }

    public float taux_interet() {
        return _taux_interet;
    }
}
