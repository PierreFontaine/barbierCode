package com.FranckBarbier.Java._Polymorphism;

public class Polymorphism {

    public static void main(String[] args) {
//        Compte_bancaire cb;
//        Compte_cheque cc = new Compte_cheque(1993,10500.F,2.F,10000.F);
//        Compte_epargne_logement cel = new Compte_epargne_logement(1996,1000.F);
//        
//        cb = cc;
//        if(cb instanceof Compte_cheque) System.out.println(cb.getClass().toString());
//        cb.appliquer_taux_interet();
//        
//        cb = cel;
//        if(cb instanceof Compte_epargne_logement) System.out.println(cb.getClass().toString());
//        cb.appliquer_taux_interet();
//        
//        Compte_cheque autre_cc = null;
//        try {
//            // autre_cc = (Compte_cheque)cb;
//            autre_cc = Compte_cheque.class.cast(cb);
//        } catch(ClassCastException cce) {
//            System.err.println("Conversion �chou�e � partir de : " + cce.getMessage());
//        } finally { // attention ici, le code amont est fait pour que le cast �choue...
//            assert(autre_cc == null);
//        }
//        
//        Compte_epargne_logement autre_cel = null;
//        // if(cb instanceof Compte_epargne_logement) autre_cel = (Compte_epargne_logement)cb;
//        try {
//            // autre_cel = (Compte_epargne_logement)cb;
//            autre_cel = Compte_epargne_logement.class.cast(cb);
//        } catch(ClassCastException cce) {
//            System.err.println("Conversion �chou�e � partir de : " + cce.getMessage());
//        } finally { // attention ici, le code amont est fait pour que le cast r�ussisse...
//            assert(autre_cel != null);
//        }
        Note cn = new Confidential_note(new java.io.StringReader("Franck Barbier"));
        try {
            System.out.println(cn.write());
        } catch (java.io.IOException ioe) {
            ioe.printStackTrace();
        }
    }
}
