package com.FranckBarbier.Java._Polymorphism;

public class Bank<T extends Compte_bancaire> {

    private java.util.Deque<T> _bank_accounts = new java.util.ArrayDeque<T>();

    public void add(final T t) {
        _bank_accounts.addLast(t);
    }

    public void absorption(Bank<? super T> bank) {
        while (!_bank_accounts.isEmpty()) {
            bank.add(_bank_accounts.removeFirst());
        }
    }

    public T greater_bank_account() {
        T result = null;
        for (java.util.Iterator<T> i = _bank_accounts.iterator(); i.hasNext();) {
            T temp = i.next();
            if (result == null) {
                result = temp;
            }
            if (temp.compareTo(result) > 0) {
                result = temp;
            }
        }
        return result;
    }

    public boolean has_bigger_customer_than(Bank<?> bank) {
        // Attention � 'greater_bank_account' qui peut retourner 'null', code � rendre plus robuste...
        return this.greater_bank_account()._solde > bank.greater_bank_account()._solde ? true : false;
    }

    public static void main(String[] args) {
        Bank<Compte_bancaire> bank = new Bank<Compte_bancaire>();
        Bank<Compte_cheque> another_bank = new Bank<Compte_cheque>();

        assert (bank.getClass() == another_bank.getClass());
        assert (bank instanceof Bank);
        assert (another_bank instanceof Bank);
        assert (bank instanceof Bank<?>);
        assert (another_bank instanceof Bank<?>);

        Compte_cheque o = new Compte_cheque(1993, 10000.F, 2.F, 10000.F);
        Compte_cheque l = new Compte_cheque(1996, 1000.F, 2.F, 10000.F);
        Compte_cheque j = new Compte_cheque(2001, 100.F, 2.F, 10000.F);
        bank.add(j);
        bank.add(l);
        bank.add(o);

        Bank<? extends Compte_bancaire> yet_another_bank = bank;
        //Bank<? extends Compte_bancaire> x = new Bank<? extends Compte_bancaire>();
//        Bank<?> yet_another_bank = another_bank;
        //yet_another_bank.ajouter(o);

//        Bank<?>[] banks = new Bank<?>[2];
//        banks[0] = bank;
//        Object[] objects = (Object[])banks;
//        float cumul_interets = banks[0].greater_bank_account().cumul_interets();                    
        Compte_cheque f = new Compte_cheque(1963, 1000000.F, 2.F, 10000.F);
        Compte_cheque s = new Compte_cheque(1964, 100000.F, 2.F, 10000.F);
        another_bank.add(s);
        another_bank.add(f);

        System.out.println("bank: " + bank.greater_bank_account()._id);
        System.out.println("autre bank: " + another_bank.greater_bank_account()._id);
        System.out.println(bank.has_bigger_customer_than(another_bank));
        System.out.println(another_bank.has_bigger_customer_than(bank));

        // bank.absorption(another_bank);
        another_bank.absorption(bank);
    }
}
