package com.FranckBarbier.Java._Polymorphism;

public class Top_secret_note extends Confidential_note {

    public final static String Default_header = "Top secret note: ";
    /*private*/ protected void initialization() {
        _text = new StringBuffer(Default_header);
    }

    public Top_secret_note(java.io.StringReader source) {
        super(source);
    }

    public String write() throws java.io.IOException {
        // encrypt();
        return super.write() + " {Unreadable!}";
    }

    private Long encrypt() {
        return 0L;
    }
}
