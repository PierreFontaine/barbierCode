package com.FranckBarbier.Java._Polymorphism;

public class OnTheFlyConcretization {

    public static void main(String[] args) {
        java.util.TimerTask timer_task = new java.util.TimerTask() {
            public void run() {
                System.out.println("A behavior...");
            }
        };
        java.util.TimerTask another_timer_task = new java.util.TimerTask() {
            public void run() {
                System.out.println("Another behavior!");
            }
        };
        timer_task.run();
        another_timer_task.run();
    }
}
