package com.FranckBarbier.Java._Primitive_types;

public class Type {

    boolean attribute0;
    byte attribute1;
    short attribute2;
    int attribute3;
    long attribute4;
    float attribute5;
    double attribute6;
    char attribute7;

    public static void main(String[] args) {
        Type object = new Type();

        object.attribute0 = true;
        System.out.println(object.attribute0); // 'true'
        object.attribute1 = '\r';
        System.out.println(object.attribute1); // '13'
        object.attribute2 = 0x7777;
        System.out.println(object.attribute2); // '30583'
        object.attribute3 = 0xFFFFFFFF;
        System.out.println(object.attribute3); // '-1'
        object.attribute4 = Long.MAX_VALUE;
        System.out.println(object.attribute4); // '9223372036854775807'
        object.attribute5 = 6.5E1F;
        System.out.println(object.attribute5); // '65.0'
        object.attribute6 = 7.2e-2D;
        System.out.println(object.attribute6); // '0.072'
        object.attribute7 = 66;
        System.out.println(object.attribute7); // 'B'
    }
}
