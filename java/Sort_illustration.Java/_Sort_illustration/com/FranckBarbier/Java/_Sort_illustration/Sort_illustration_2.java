package com.FranckBarbier.Java._Sort_illustration;

public class Sort_illustration_2<T extends Comparable<T>> {

    java.util.Vector<T> _representation;

    public enum Speed {

        Slow, Quick
    }
    int _cost; // différence avec C++ notable

    void sort(int left, int right) {
        assert (left < right);
        for (int i = left; i < right; i++) {
            for (int j = i + 1; j <= right; j++) {
                _cost++;
                if (_representation.elementAt(i).compareTo(_representation.elementAt(j)) > 0) {
                    T temp = _representation.elementAt(i);
                    _representation.setElementAt(_representation.elementAt(j), i);
                    _representation.setElementAt(temp, j);
                }
            }
        }
    }

    int split(int left, int right) {
        int pivot = left;
        T value = _representation.elementAt(left);
        for (int i = left + 1; i <= right; i++) {
            _cost++;
            if (_representation.elementAt(i).compareTo(value) < 0) {
                pivot++;
                T temp = _representation.elementAt(pivot);
                _representation.setElementAt(_representation.elementAt(i), pivot);
                _representation.setElementAt(temp, i);
            }
        }
        T temp = _representation.elementAt(left);
        _representation.setElementAt(_representation.elementAt(pivot), left);
        _representation.setElementAt(temp, pivot);
        return pivot;
    }

    void quick_sort(int left, int right) {
        if (right - left <= 0) {
            return;
        }
        int pivot = split(left, right);
        quick_sort(left, pivot - 1);
        quick_sort(pivot + 1, right);
    }

    public Sort_illustration_2(int size) {
        assert (size > 0);
        _representation = new java.util.Vector<T>(size);
    }

    public boolean add(T t) {
        return _representation.add(t);
    }

    public int sort(Speed s) {
        _cost = 0;
        switch (s) {
            case Slow:
                sort(0, _representation.size() - 1);
                break;
            case Quick:
                quick_sort(0, _representation.size() - 1);
                break;
            default:
                ;
        }
        return _cost;
    }

    public static void main(String[] args) {
        int n = 10000;
        Sort_illustration_2<Integer> si = new Sort_illustration_2<Integer>(n);
        java.util.Random random = new java.util.Random((new java.util.Date()).getTime());
        for (int i = 0; i < n; i++) {
            si.add(new Integer(random.nextInt()));
        }
        System.out.println(si.sort(Speed.Slow));
        System.out.print("n*log(n): " + n * Math.log(n));
    }
}
