package com.FranckBarbier.Java._Sort_illustration;

public class Sort_illustration {

    int _size;
    int _representation[];

    public enum Speed {

        Slow, Quick
    }
    int _cost; // différence avec C++ notable

    void sort(int left, int right) {
        assert (left < right);
        for (int i = left; i < right; i++) {
            for (int j = i + 1; j <= right; j++) {
                _cost++;
                if (_representation[i] > _representation[j]) {
                    int temp = _representation[i];
                    _representation[i] = _representation[j];
                    _representation[j] = temp;
                }
            }
        }
    }

    int split(int left, int right) {
        int pivot = left;
        int value = _representation[left];
        for (int i = left + 1; i <= right; i++) {
            _cost++;
            if (_representation[i] < value) {
                pivot++;
                int temp = _representation[pivot];
                _representation[pivot] = _representation[i];
                _representation[i] = temp;
            }
        }
        int temp = _representation[left];
        _representation[left] = _representation[pivot];
        _representation[pivot] = temp;
        return pivot;
    }

    void quick_sort(int left, int right) {
        if (right - left <= 0) {
            return;
        }
        int pivot = split(left, right);
        quick_sort(left, pivot - 1);
        quick_sort(pivot + 1, right);
    }

    public Sort_illustration(int size) {
        assert (size > 0);
        _size = size;
        _representation = new int[_size];
        java.util.Random random = new java.util.Random((new java.util.Date()).getTime());
        for (int i = 0; i < _size; i++) {
            _representation[i] = random.nextInt();
        }
    }

    public int sort(Speed s) {
        _cost = 0;
        switch (s) {
            case Slow:
                sort(0, _size - 1);
                break;
            case Quick:
                quick_sort(0, _size - 1);
                break;
            default: ;
        }
        return _cost;
    }

    public static void main(String[] args) {
        int n = 10000;
        Sort_illustration si = new Sort_illustration(n);
        System.out.println(si.sort(Speed.Slow));
        System.out.print("n*log(n): " + n * Math.log(n));
    }
}
