package com.FranckBarbier.Java._Temperature;

public class Invalid_temperature_exception extends Exception {

    private float _value; // In Celsius

    public Invalid_temperature_exception(float value) {
        super("Invalid temperature");
        _value = value;
    }

    public float value() {
        return _value;
    }
}
