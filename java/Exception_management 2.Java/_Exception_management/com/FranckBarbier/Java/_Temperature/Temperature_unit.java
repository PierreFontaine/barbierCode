package com.FranckBarbier.Java._Temperature;

public enum Temperature_unit {

    Celsius("�C"), Fahrenheit("�F"), Kelvin("�K");
    protected final String _literal;

    Temperature_unit(String literal) {
        _literal = literal;
    }

    public String toString() {
        return _literal;
    }
}
