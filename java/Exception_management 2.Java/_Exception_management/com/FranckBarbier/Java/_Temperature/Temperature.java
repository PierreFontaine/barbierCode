package com.FranckBarbier.Java._Temperature;

public final class Temperature implements Cloneable {

    public enum Temperature_unit {

        Celsius, Fahrenheit, Kelvin
    }
//    public static final byte Celsius = 0;
//    public static final byte Fahrenheit = 1;
//    public static final byte Kelvin = 2;
//    public static final String[] Literals = new String[3];
//    static {
//        Literals[Celsius] = new String("°C");
//        Literals[Fahrenheit] = new String("°F");
//        Literals[Kelvin] = new String("°K");
//    }
    public static final float Min = -273.15F; // in Celsius
    private float _value; // in Celsius
    private float _step;

    public float asCelsius() {
        return _value;
    }

    public float asFahrenheit() {
        return 9.F / 5.F * _value + 32.F;
    }

    public float asKelvin() {
        return _value - Min;
    }
//    public String asStringCelsius() {
//        return String.valueOf(asCelsius()) + Literals[Celsius];
//    }
//    public String asStringFahrenheit() {
//        return String.valueOf(asFahrenheit()) + Literals[Fahrenheit];
//    }
//    public String asStringKelvin() {
//        return String.valueOf(asKelvin()) + Literals[Kelvin];
//    }

    public Object clone() throws CloneNotSupportedException {
        // 'implements Cloneable' avoids the raising of an instance of CloneNotSupportedException
        // cloning amounts then to field-by-field copy
        return super.clone();
    }

    public void decrement() throws Invalid_temperature_exception {
        _value -= _step;
        if (_value < Min) {
            throw new Invalid_temperature_exception(_value);
        }
//        assert(_value >= Min);
    }

    public void increment() {
        _value += _step;
    }

    public boolean equals(Temperature t) {
        return _value == t._value;
    }

    public boolean greaterThan(Temperature t) {
        return _value > t._value;
    }

    public boolean greaterOrEqualThan(Temperature t) {
        return _value >= t._value;
    }

    public boolean lessThan(Temperature t) {
        return _value < t._value;
    }

    public boolean lessOrEqualThan(Temperature t) {
        return _value <= t._value;
    }

    public Temperature() {
        _value = 0.F;
        _step = 0.0001F;
    }

    public Temperature(float value, Temperature_unit unit) throws Exception/*,Invalid_temperature_exception*/ {
        // this();
        switch (unit) {
            case Celsius:
                _value = value;
                break;
            case Fahrenheit:
                _value = (value - 32.F) * 5.F / 9.F;
                break;
            case Kelvin:
                _value = value + Min;
                break;
            default:
                throw new Exception("Illegal temperature unit");
        }
        if (_value < Min) {
            throw new Invalid_temperature_exception(_value);
        }
        _step = 0.0001F;
    }
//    public String toString(Temperature_unit temperature_unit) {
//        switch(temperature_unit) {
//            case Celsius: return String.valueOf(asCelsius()) + "�C";
//            case Fahrenheit: return String.valueOf(asFahrenheit()) + "�F";
//            case Kelvin: return String.valueOf(asKelvin()) + "�K";
//            default: return "";
//        }
//    }

    public String toString(Temperature_unit temperature_unit) {
        switch (temperature_unit) {
            case Celsius:
                return String.valueOf(asCelsius()) + temperature_unit.toString();
            case Fahrenheit:
                return String.valueOf(asFahrenheit()) + temperature_unit.toString();
            case Kelvin:
                return String.valueOf(asKelvin()) + temperature_unit.toString();
            default:
                return "";
        }
    }
}
