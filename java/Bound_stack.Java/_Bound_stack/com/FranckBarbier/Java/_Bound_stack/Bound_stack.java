package com.FranckBarbier.Java._Bound_stack;

public class Bound_stack<T> extends java.util.ArrayDeque<T> {

    private final int _capacity;

    Bound_stack(final int capacity) {
        _capacity = capacity;
    }

    public boolean full() {
        return _capacity == this.size();
    }

    public void push(final T t) {
        if (full()) {
            throw new RuntimeException("push");
        }
        super.push(t);
    }

    public static void main(String[] args) {
        Bound_stack<Character> bs = new Bound_stack<Character>(10);
        bs.push('A');
        bs.push('B');
        // bs.removeFirst(); // 'A' would be removed while it's illegal for a stack!
        bs.pop(); // 'A'
    }
}
