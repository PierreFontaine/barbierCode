package My_class;

class My_class_mother {

    final static void My_method() {
        System.out.println("I'm both a final and static method in " + My_class_mother.class.getSimpleName() + ", what does it mean?");
    }
}

public class My_class extends My_class_mother {

//    static void my_method() { // Static methods can be 'hidden' instead of 'overridden', but 'final' in 'My_class_mother' prevents hidding here
//    }
    public static void main(String[] args) {
        My_class.My_method(); // Normal style, 'final' in 'My_class_mother' prevents any hidding in 'My_class'
        My_class mc = new My_class();
        mc.My_method(); // Weird Java style through instances!
    }
}
