package com.FranckBarbier.Java._Heap;

public class Task implements java.util.Comparator<Task> {

    private int _priority;

    public Task(int priority) {
        _priority = priority;
    }

    @Override
    public int compare(Task t1, Task t2) {
        if (t1._priority < t2._priority) {
            return -1;
        }
        if (t1._priority == t2._priority) {
            return 0;
        }
        return 1;
    }
}
