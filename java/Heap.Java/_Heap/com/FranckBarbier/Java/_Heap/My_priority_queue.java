package com.FranckBarbier.Java._Heap;

public class My_priority_queue<T> {

    private java.util.Vector<T> _content = new java.util.Vector<T>();

    public void insert(T t) {
        _content.addElement(t);
        int i = _content.size() - 1;
        for (int j = i / 2; i > 0 && _content.get(j).hashCode() < _content.get(i).hashCode(); j /= 2) {
            T temp = _content.get(i);
            _content.set(i, _content.get(j));
            _content.set(j, temp);
            i = j;
        }
    }

    public void sort(java.util.Vector<T> result) {
        assert (result != null);
        result.addAll(_content);
        assert (result.size() > 1);
        for (int i = result.size() - 1; i > 0; i--) {
            T temp = result.get(i);
            result.set(i, result.get(0));
            result.set(0, temp);
            int max;
            for (int j = 0; 2 * j + 1 < i; j = max) {
                if (2 * j + 2 == i) {
                    max = 2 * j + 1;
                } else {
                    max = result.get(2 * j + 1).hashCode() > result.get(2 * j + 2).hashCode() ? 2 * j + 1 : 2 * j + 2;
                }
                if (result.get(max).hashCode() > result.get(j).hashCode()) {
                    temp = result.get(max);
                    result.set(max, result.get(j));
                    result.set(j, temp);
                }
            }
        }
    }

    public static void main(String[] args) {

        My_priority_queue<Short> my_priority_queue = new My_priority_queue<Short>();
        my_priority_queue.insert((short) 13);
        my_priority_queue.insert((short) 7);
        my_priority_queue.insert((short) 12);
        my_priority_queue.insert((short) 4);
        my_priority_queue.insert((short) 6);
        my_priority_queue.insert((short) 11);
        my_priority_queue.insert((short) 8);
        my_priority_queue.insert((short) 3);
        my_priority_queue.insert((short) 9);
        java.util.Vector<Short> result = new java.util.Vector<Short>();
        my_priority_queue.sort(result);
        for (int i = 0; i < result.size(); i++) {
            System.out.print("\t" + i + ": " + result.get(i));
        }

        System.out.print("\n***\n");

        java.util.Comparator<Short> comparator = new java.util.Comparator<Short>() {
            @Override
            public int compare(Short i, Short j) {
                if (i < j) {
                    return -1;
                }
                if (i == j) {
                    return 0;
                }
//                if (i > j) {
//                    return 1;
//                }
                return 1; // Mandatory without 'if'
            }
        };
        java.util.PriorityQueue<Short> priority_queue = new java.util.PriorityQueue<Short>(10, comparator);
        priority_queue.add((short) 13);
        priority_queue.add((short) 7);
        priority_queue.add((short) 12);
        priority_queue.add((short) 4);
        priority_queue.add((short) 6);
        priority_queue.add((short) 11);
        priority_queue.add((short) 8);
        priority_queue.add((short) 3);
        priority_queue.add((short) 9);
        for (java.util.Iterator<Short> i = priority_queue.iterator(); i.hasNext();) {
            System.out.print("\t" + i.next());
        }
    }
}
