package com.FranckBarbier.Java._Generics;

public class Task implements Object_with_priority<Task> {

    final private Integer _priority;

    @Override
    public int get_priority() {
        return _priority;
    }

    public Task(int priority) {
        _priority = priority;
    }

    @Override
    public int compareTo(Task task) {
        return _priority.compareTo(task._priority);
    }
}
