package com.FranckBarbier.Java._Generics;

public class Text_from_right_to_left extends Text<StringBuffer> {

    public Text_from_right_to_left(StringBuffer sb1, StringBuffer sb2) {
        super(sb1, sb2);
    }

    public StringBuffer premier() {
        return _content.firstElement().reverse();
    }

    public static void main(String[] args) {
        Text<StringBuffer> t = new Text_from_right_to_left(new StringBuffer("Franck"), new StringBuffer("Barbier"));
        System.out.println(t.first());

        java.lang.reflect.Method[] methods = Text_from_right_to_left.class.getDeclaredMethods();
        for (java.lang.reflect.Method m : methods) {
            System.out.println(m.getName() + " is bridge? " + m.isBridge());
        }
    }
}
