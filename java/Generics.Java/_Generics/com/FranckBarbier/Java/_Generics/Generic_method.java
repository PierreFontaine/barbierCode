package com.FranckBarbier.Java._Generics;

public class Generic_method {

    public static void main(String[] args) {
        Short tab[] = {13, 7, 6, 4, 12, 11, 8, 3}; // 'short tab[]' does not work!
        java.util.Collections.sort(java.util.Arrays.asList(tab)); // 'tab' is sorted!
        for (Short s : tab) {
            System.out.print("-" + s);
        }

        System.out.print("\n\n");

        java.util.Deque<Short> deque = new java.util.ArrayDeque<Short>();
        deque.addLast((short) 13);
        deque.addLast((short) 7);
        deque.addLast((short) 6);
        deque.addLast((short) 4);
        deque.addLast((short) 12);
        deque.addLast((short) 11);
        deque.addLast((short) 8);
        deque.addLast((short) 3);
        // Short tab[] = (Short[])deque.toArray();

        Short result[] = deque.toArray(new Short[0]);
        for (Short s : result) {
            System.out.print("-" + s);
        }

        System.out.print("\n\n");

        java.util.List<Short> list = new java.util.ArrayList<Short>();
        list.add((short) 13);
        list.add((short) 7);
        list.add((short) 6);
        list.add((short) 4);
        list.add((short) 12);
        list.add((short) 11);
        list.add((short) 8);
        list.add((short) 3);
        java.util.Collections.sort(list);
        for (Short s : list) {
            System.out.print("-" + s);
        }

//        java.util.List<Task> other_list = new java.util.ArrayList<Task>();
//        java.util.Collections.sort(other_list);
        java.util.List<Object_with_priority> other_list = new java.util.ArrayList<Object_with_priority>();
        Object_with_priority owp = new Task(0);
        other_list.add(owp);
        java.util.Collections.sort(other_list);
    }
}
