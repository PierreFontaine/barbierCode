package com.FranckBarbier.Java._Generics;

public class Any_container<T> {
//    T generic_array_of_T[]; // No d'allocation through 'new' -> no compilation error!

    java.util.Vector<T> generic_array_of_T = new java.util.Vector<T>();
//    public void f() {
//        Object[] tab = generic_array_of_T;
//        tab[0] = "Franck";
//    }

    public void copyTo(java.util.Collection collection) {
        for (T t : generic_array_of_T) {
            collection.add(t);
        }
    }

    public void insert(T t) {
        generic_array_of_T.add(t);
    }

    public static void main(String[] args) {
        Any_container<Integer> ac = new Any_container<Integer>();
        ac.insert(1963);
        java.util.Vector<String> vector = new java.util.Vector<String>();
//        ac.copyTo(vector);
//        String s = (String)vector.firstElement();      
        java.util.Collection<String> temp = java.util.Collections.checkedCollection(vector, String.class);
        ac.copyTo(java.util.Collections.checkedCollection(vector, String.class));
    }
}
