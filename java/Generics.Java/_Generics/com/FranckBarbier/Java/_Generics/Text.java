package com.FranckBarbier.Java._Generics;

public class Text<Element extends CharSequence> {

    protected java.util.Vector<Element> _content = new java.util.Vector<Element>();

    public void f() {
    }

    public Text(Element e1, Element e2) {
        _content.addElement(e1);
        _content.addElement(e2);
    }

    public Element first() {
        return _content.firstElement();
    }
}
