package com.FranckBarbier.Java._Generics;

public interface Object_with_priority<T extends Object_with_priority> extends Comparable<T> {

    int get_priority();
}
