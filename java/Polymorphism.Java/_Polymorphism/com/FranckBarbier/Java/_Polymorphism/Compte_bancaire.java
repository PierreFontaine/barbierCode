package com.FranckBarbier.Java._Polymorphism;

abstract public class Compte_bancaire implements Comparable<Compte_bancaire> {

    int _id;
    protected float _solde;
    protected float _cumul_interets;

    protected Compte_bancaire(int id, float solde) {
        _id = id;
        _solde = solde;
    }

    public int id() {
        return _id;
    }

    public float solde() {
        return _solde;
    }

    public float cumul_interets() {
        return _cumul_interets;
    }

    public float mise_a_jour(float montant) {
        _solde += montant;
        return _solde;
    }

    abstract public float taux_interet();

    public void appliquer_taux_interet() {
        _cumul_interets = _solde * (1.F + (taux_interet() / 100.F));
    }

    public int compareTo(Compte_bancaire cb) {
        return _solde > cb._solde ? 1 : _solde < cb._solde ? -1 : 0;
    }
}
