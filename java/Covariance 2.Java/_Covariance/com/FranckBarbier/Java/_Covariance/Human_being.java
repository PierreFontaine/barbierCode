package com.FranckBarbier.Java._Covariance;

public class Human_being {

//    private java.util.Calendar _birth_date;
    java.util.Calendar _birth_date;
//    protected java.util.Calendar _birth_date;

    public Human_being(int day_of_month, int month, int year) {
        _birth_date = new java.util.GregorianCalendar(year, month, day_of_month);
    }

    public int age() {
        return (new java.util.GregorianCalendar()).get(java.util.Calendar.YEAR) - _birth_date.get(java.util.Calendar.YEAR);
    }

    public Human_being cloning() {
        System.out.print("cloning in Human_being...\n");
        return new Human_being(_birth_date.get(java.util.Calendar.DAY_OF_MONTH), _birth_date.get(java.util.Calendar.MONTH), _birth_date.get(java.util.Calendar.YEAR));
    }
}
