package com.FranckBarbier.Java._Covariance;

public class Female extends Human_being {

    public Female(int day_of_month, int month, int year) {
        super(year, month, day_of_month);
    }

    public Female cloning() {
        System.out.print("cloning in Female...\n");
        return new Female(_birth_date.get(java.util.Calendar.DAY_OF_MONTH), _birth_date.get(java.util.Calendar.MONTH), _birth_date.get(java.util.Calendar.YEAR));
    }
}
