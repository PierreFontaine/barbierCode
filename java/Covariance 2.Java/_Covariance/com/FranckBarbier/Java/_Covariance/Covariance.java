package com.FranckBarbier.Java._Covariance;

public class Covariance {

    public static void main(String[] args) {
        Human_being FranckBarbier = new Male(11, 1, 1963);
        Human_being clone = FranckBarbier.cloning(); // 'cloning in Male...' is displayed => covariance applies in Java!

        Family family = new Family();
        // family.births(new Human_being(6,12,1993),new Human_being(15,4,1996),new Human_being(22,2,2001));
        Human_being kids[] = {new Human_being(6, 12, 1993), new Human_being(15, 4, 1996), new Human_being(22, 2, 2001)};
        family.births(kids);
        System.out.println(family.last_child_birth_year());
    }
}
