package com.FranckBarbier.Java._Covariance;

public class Male extends Human_being {

    public Male(int day_of_month, int month, int year) {
        super(year, month, day_of_month);
    }

    public Male cloning() {
        System.out.print("cloning in Male...\n");
        return new Male(_birth_date.get(java.util.Calendar.DAY_OF_MONTH), _birth_date.get(java.util.Calendar.MONTH), _birth_date.get(java.util.Calendar.YEAR));
    }
}
