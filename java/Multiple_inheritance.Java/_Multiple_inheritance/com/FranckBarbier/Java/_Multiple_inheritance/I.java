package com.FranckBarbier.Java._Multiple_inheritance;

public interface I {

    String S = "S in I";

    void f();

    void g(I i);

    I h();
}
