package com.FranckBarbier.Java._Multiple_inheritance;

public interface J {

    String S = "S in J";

    void f();

    void g(J j);

    J h();
}
