package com.FranckBarbier.Java._Multiple_inheritance;

public class C implements K {

    public void f() { // Compilation error about 'g': 'g' from 'I' or 'g' from 'J'?
        g(this);
    }

    public void g(I i) {
    }

    public void g(J j) {
    }

    public static void main(String[] args) {
        System.out.println(I.S);
    }
}
