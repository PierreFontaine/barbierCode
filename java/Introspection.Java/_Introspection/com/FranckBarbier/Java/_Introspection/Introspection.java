package com.FranckBarbier.Java._Introspection;

import com.FranckBarbier.Java._Temperature.Temperature;

public class Introspection {

    public static void main(String[] args) {
        try {
            System.out.println("Absence of infinite instantiation graph regression in Java: " + Class.class.getName());

            Class<?> class_of_Temperature = Temperature.class;
            System.out.println("Class of 'class_of_Temperature': " + class_of_Temperature.getName());
            Class<?> class_of_Temperature_unit = Temperature.Temperature_unit.class;
            System.out.println("Class of 'class_of_Temperature_unit': " + class_of_Temperature_unit.getName());
            assert (class_of_Temperature == class_of_Temperature_unit.getEnclosingClass());
            Temperature ambient_temperature = new Temperature(18.F, Temperature.Temperature_unit.Celsius);
            assert (class_of_Temperature == ambient_temperature.getClass());
            for (Object object : class_of_Temperature_unit.getEnumConstants()) {
                System.out.println(object);
            }

            try {
                Temperature.Temperature_unit.class.newInstance();
            } catch (InstantiationException ie) {
                System.err.println("Failure when dynamically creating a value of this enumerated type: " + Temperature.Temperature_unit.class);
            } catch (IllegalAccessException iae) {
                iae.printStackTrace();
            }

            java.lang.reflect.Method methods[] = ambient_temperature.getClass().getMethods();
            for (int i = 0; i < methods.length; i++) {
                Class<?> types_of_parameters[] = methods[i].getParameterTypes();
                Object arguments[] = null;
                if (methods[i].getName().equals("toString") && methods[i].isVarArgs() == false && methods[i].getParameterTypes().length == 0) {
                    System.out.println("Calling 'toString' by introspection on 'ambient_temperature': " + methods[i].invoke(ambient_temperature, arguments));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
