package com.FranckBarbier.Java._Temperature;

public final class Temperature implements Cloneable, java.io.Serializable {

    public final static float Min = -273.15F; // in Celsius

    public enum Temperature_unit {

        Celsius, Fahrenheit, Kelvin
    }
    private float _value; // in Celsius
    private float _step;

    public Temperature() {
        _value = 0.F;
        _step = 0.0001F;
    }

    public Temperature(float value, Temperature_unit temperature_unit) throws Exception {
        this();
        switch (temperature_unit) {
            case Celsius:
                _value = value;
                break;
            case Fahrenheit:
                _value = (value - 32.F) * 5.F / 9.F;
                break;
            case Kelvin:
                _value = value + Min;
                break;
            default:
                throw new Exception("Illegal temperature unit");
        }
        if (_value < Min) {
            throw new Exception("Illegal temperature value");
        }
    }

    public Temperature(float value, Temperature_unit temperature_unit, float step) throws Exception {
        this(value, temperature_unit);
        _step = step;
    }

    public float asCelsius() {
        return _value;
    }

    public float asFahrenheit() {
        return 9.F / 5.F * _value + 32.F;
    }

    public float asKelvin() {
        return _value - Min;
    }

    public String asStringCelsius() {
        return String.valueOf(asCelsius()) + "�C";
    }

    public String asStringFahrenheit() {
        return String.valueOf(asFahrenheit()) + "�F";
    }

    public String asStringKelvin() {
        return String.valueOf(asKelvin()) + "�K";
    }

    public Object clone() throws CloneNotSupportedException {
        // 'implements Cloneable' avoids the raising of an instance of 'CloneNotSupportedException'
        // Cloning amounts then to field-by-field copy
        return super.clone();
    }

    public void decrement() throws Exception {
        _value -= _step;
        if (_value < Min) {
            throw new Exception("Illegal temperature value");
        }
    }

    public void increment() {
        _value += _step;
    }

    public boolean equals(Temperature t) {
        return _value == t._value;
    }

    public boolean greaterThan(Temperature t) {
        return _value > t._value;
    }

    public boolean greaterOrEqualThan(Temperature t) {
        return _value >= t._value;
    }

    public boolean lessThan(Temperature t) {
        return _value < t._value;
    }

    public boolean lessOrEqualThan(Temperature t) {
        return _value <= t._value;
    }

    @Override
    public String toString() {
        return asStringCelsius();
    }
}
