package com.FranckBarbier.Java._Multithreading;

public class Simple_dead_lock {

    private int i = 0;

    synchronized public void f() {
        System.err.println("Start of 'f()'");
        i++;
        g();
        System.err.println("Value of 'i': " + i);
        notify();
    }

    synchronized private void g() {
        System.err.println("Start of 'g()'");
        try {
            wait(5000); // Dead lock occurs for 5 sec.!
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }
        System.err.println("End of 'wait()'");
    }

    public static void main(String[] args) {
        Simple_dead_lock sdl = new Simple_dead_lock();
        sdl.f();
    }
}
