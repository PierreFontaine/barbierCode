package com.FranckBarbier.Java._Multithreading;

public class Illustration_of_volatile {

    volatile private boolean _stop = false;

    /* volatile */    private String _given_name = "?";
//    java.util.concurrent.atomic.AtomicReference<String> _given_name = new java.util.concurrent.atomic.AtomicReference<String>("?");
    /* volatile */    private String _surname = "?";
//    java.util.concurrent.atomic.AtomicReference<String> _surname = new java.util.concurrent.atomic.AtomicReference<String>("?");
    private Thread _t1, _t2, _t3;

    private void set(String given_name, String surname) {
        _given_name = given_name;
//        _given_name.set(given_name);
        _surname = surname;
//        _surname.set(surname);
    }

    private void print() {
        if (_given_name.equals("Fran" + (char) 0x00E7 + "ois") && _surname.equals("Sarkozy")) {
            System.out.println("Fran" + (char) 0x00E7 + "ois" + " " + "Sarkozy");
        }
        if (_given_name.equals("Nicolas") && _surname.equals("Hollande")) {
            System.out.println("Nicolas" + " " + "Hollande");
        }
    }

    public Illustration_of_volatile() {
        _t1 = new Thread() {
            public void run() {
                while (!_stop) {
                    set("Fran" + (char) 0x00E7 + "ois", "Hollande");
                }
            }
        };
        _t2 = new Thread() {
            public void run() {
                while (!_stop) {
                    set("Nicolas", "Sarkozy");
                }
            }
        };
        _t3 = new Thread() {
            public void run() {
                while (!_stop) {
                    print();
                }
            }
        };
    }

    public void start() {
        _t1.start();
        _t2.start();
        _t3.start();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }
        _stop = true;
        _t1 = null;
        _t2 = null;
        _t3 = null;
    }

    public static void main(String[] args) {
        new Illustration_of_volatile().start();
    }
}
