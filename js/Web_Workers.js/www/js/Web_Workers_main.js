/*
 * Web_Workers_main.js
 */

"use strict";

function main() {

    var $canvas = $("#My_canvas").get(0); // jQuery access to DOM canvas elem. with 'My_canvas' as id.
    var image = new Image();
    var worker = new Worker("js/Web_Workers_parallel.js"); // Behind the browser stage, this "parallel" code is that of the worker...

    worker.addEventListener("message", response_from_process_image_by_worker); // Subscription about worker's response
    function response_from_process_image_by_worker(message) { // Handler of worker's response
        swal({// Use of the nice 'sweetalert' JS library!
            title: "Web Workers",
            text: "Worker just terminated image processing... Show it?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes!",
            closeOnConfirm: true
        }, function () {
            $canvas.getContext('2d').putImageData(message.data, 0, 0); // Reload of processed image...
        });
        worker.terminate(); // Worker is no longer used, 'close' optional statement
    }

    $(document).on("go!", process_image_by_worker); // Wait for image availability
    function process_image_by_worker() { // Handler of image availability
        if (window.Worker) { // Test if the browser supports the Web Workers technology
            // Instance of 'ImageData' must be sent since it is processable by the new HTML5 structured clone algorithm:
            worker.postMessage($canvas.getContext('2d').getImageData(0, 0, $canvas.width, $canvas.height));
        }
    }

    image.onload = function () {
        $canvas.width = image.width;
        $canvas.height = image.height;
        $canvas.getContext('2d').drawImage(image, 0, 0); // Image is loaded in canvas
        $(document).trigger("go!"); // Image availability: event triggering in jQuery
    };
    image.src = "img/Image.jpg"; // Image load...
}
