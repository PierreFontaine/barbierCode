/*
 * Temperature_main.js
 */

"use strict";
function main() {

    var temperature = {
        Min: -273.15, // in Celsius
        _value: 0, // in Celsius
        _step: 0.0001,
        asCelsius: function () {
            return _value;
        }
        // Etc.
    };

    var Temperature_unit = {
        Celsius: 0,
        Fahrenheit: 1,
        Kelvin: 2
    };

    function Invalid_temperature_exception(value) {
        this._message = "Invalid temperature";
        this._value = value;
    }

    var Temperature = function (value, unit) {
        this.Min = -273.15; // in Celsius
        this._value = 0; // in Celsius
        switch (unit) {
            case Temperature_unit.Celsius:
                this._value = value;
                break;
            case Temperature_unit.Fahrenheit:
                this._value = (value - 32.) * 5. / 9.;
                break;
            case Temperature_unit.Kelvin:
                this._value = value + this.Min;
                break;
            default:
                throw "Illegal temperature unit";
        }
        if (this._value < this.Min) {
            throw new Invalid_temperature_exception(this._value);
        }
        this._step = 0.0001;
        this.asCelsius = function () {
            return this._value;
        };
        this.asFahrenheit = function () {
            return 9. / 5. * this._value + 32.;
        };
        Object.defineProperty(this, "asKelvin", {value: function () {
                return this._value - this.Min;
            }, enumerable: true, configurable: false, writable: false});
        // Etc.     
    };

    var t1 = new Temperature(0, Temperature_unit.Celsius);
//    window.alert(t1 instanceof Temperature); // 'true'
//    window.alert(JSON.stringify(t1));
//    window.alert(t1.asFahrenheit());
    window.alert(t1.asKelvin());


    var t2 = new Temperature(32., Temperature_unit.Fahrenheit);
    t2.asCelsius = t2.asFahrenheit;
    t2.asFahrenheit = t1.asCelsius;
    window.alert(t2.asFahrenheit());
}
