/*
 * Temperature_main2.js
 */

"use strict";
function main() {

    var Temperature_unit = {
        Celsius: 0,
        Fahrenheit: 1,
        Kelvin: 2
    };

    function Invalid_temperature_exception(value) {
        this._message = "Invalid temperature";
        this._value = value;
    }

    var Temperature = function (value, unit) { // Constructor
        this._value = 0; // in Celsius
        switch (unit) {
            case Temperature_unit.Celsius:
                this._value = value;
                break;
            case Temperature_unit.Fahrenheit:
                this._value = (value - 32.) * 5. / 9.;
                break;
            case Temperature_unit.Kelvin:
                this._value = value + this.Min;
                break;
            default:
                throw "Illegal temperature unit";
        }
        if (this._value < this.Min) {
            throw new Invalid_temperature_exception(this._value);
        }
        this._step = 0.0001;
    };

    Object.defineProperty(Temperature.prototype, "Min", {value: -273.15, enumerable: true, configurable: false, writable: false});

    Temperature.prototype.asCelsius = function () {
        return this._value;
    };

    Temperature.prototype.asFahrenheit = function () {
        return 9. / 5. * this._value + 32.;
    };

    Object.defineProperty(Temperature.prototype, "asKelvin", {value: function () {
            return this._value - this.Min;
        }, enumerable: true, configurable: false, writable: false});
    // Etc.     

    var t1 = new Temperature(0, Temperature_unit.Kelvin);
    window.alert(t1.asCelsius());
    var t2 = new Temperature(64., Temperature_unit.Fahrenheit);
    window.alert(t2.asCelsius());
}
