"use strict";

// Common usage (2 lines are displayed in the console):
// document.onreadystatechange = function () {
//    if (document.readyState === "interactive")
//        console.log('\'document.readyState\' is: ' + document.readyState + '.\n' +
//                'However, image width may... or may NOT YET defined: ' + document.getElementById("Promise_illustration").width);
//
//    if (document.readyState === "complete")
//        console.log('\'document.readyState\' is: ' + document.readyState + '.\n' +
//                'However, image width may... or may NOT YET defined: ' + document.getElementById("Promise_illustration").width);
//};

let DOM_is_usable;
chai.assert.isUndefined(DOM_is_usable, 'Strange? \'DOM_is_usable\' is NOT undefined...')
// French: UNE seule fonction (nommée 'f' ici mais elle est souvent anonyme, i.e., sans nom) est passée en paramètre
// avec (jusqu'à) DEUX arguments (un seul ici appelé 'function_launched_when_DOM_is_usable'), arguments *qui sont eux-mêmes des fonctions*
let p1 = new Promise(function f(function_launched_when_DOM_is_usable) {
    DOM_is_usable = function_launched_when_DOM_is_usable;
}).then(value => { // Anonymous function as parameter of 'then'. This function has itself 'value' as parameter...
    chai.assert.isTrue(value instanceof Event, 'value instanceof Event');
    chai.assert.strictEqual(value.type, "readystatechange", 'value.type === "readystatechange"');
    // The promise is resolved one time only! So, 'document.readyState === "complete"' CANNOT occur;
    chai.assert.strictEqual(document.readyState, "interactive", 'Strange? \'document.readyState\' is NOT properly set up...');
//    alert('\'document.readyState\' is: ' + document.readyState + '.\n' +
//            'Image width MAY... or MAY NOT yet be defined: ' + document.getElementById("Promise_illustration").width);
});
document.onreadystatechange = DOM_is_usable; // Caution: it captures the move from "loading" to "interactive" AND from "interactive" to "complete" 
// Alternative:
// document.addEventListener('DOMContentLoaded', DOM_is_usable); // <=> 'document.readyState === "interactive"'

let Window_is_loaded;
chai.assert.isUndefined(Window_is_loaded, 'Strange? \'Window_is_loaded\' is NOT undefined...')
let p2 = new Promise(function_launched_when_Window_is_loaded => {
    Window_is_loaded = function_launched_when_Window_is_loaded;
}).then(value => { // Anonymous function as parameter of 'then'. This function has itself 'value' as parameter...
    chai.assert.isTrue(value instanceof Event, 'value instanceof Event');
    chai.assert.strictEqual(value.type, "load", 'value.type === "load"');
    chai.assert.strictEqual(document.readyState, "complete", 'Strange? \'document.readyState\' is NOT properly set up...');
//    alert('\'document.readyState\' is: ' + document.readyState + '.\n' +
//            'However, image properties are ready to use!');
});
window.addEventListener('load', Window_is_loaded);

Promise.all([p1, p2]).then(value => {
    // 'value' is an array of results provided by 'p1' and 'p2':
    chai.assert.isTrue(value instanceof Array, 'value instanceof Array');
    chai.assert.strictEqual(document.readyState, "complete", 'Strange? \'document.readyState\' is NOT properly set up...');
    alert('Everything is now ready for the Web!');
});

