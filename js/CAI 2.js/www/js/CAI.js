/*
 * CAI.js
 */

'use strict';

class Compte_bancaire_JS6 {
    constructor(id, solde) {
        this._id = id;
        this._solde = solde;
        this._cumul_interets;
    }
    static Information() {
        return "Classe générale des comptes bancaires";
    }
    id() {
        return this._id;
    }
    solde() {
        return this._solde;
    }
    cumul_interets() {
        return this._cumul_interets;
    }
    mise_a_jour(montant) {
        this._solde += montant;
        return this._solde;
    }
    taux_interet() {
        throw "Undefined function due to abstract nature of the class...";
    }
    appliquer_taux_interet() {
        this._cumul_interets = this._solde * (1. + (this.taux_interet() / 100.));
    }
    compareTo(cb) {
        return this._solde > cb._solde ? 1 : this._solde < cb._solde ? -1 : 0;
    }
}

class Compte_cheque_JS6 extends Compte_bancaire_JS6 {
    constructor(id, solde, taux_interet, seuil) {
        super(id, solde);
        this._taux_interet = taux_interet;
        this._seuil = seuil;
    }
    static Information() {
        return "Classe des comptes bancaires courants ou \"comptes chèque\" - rémunération à la tête du client !";
    }
    taux_interet() {
        return this._taux_interet;
    }
    appliquer_taux_interet() {
        if (this._solde > this._seuil) {
            super.appliquer_taux_interet();
        }
    }
}

function go() {
    var stage = new createjs.Stage("CAI");
    var circle = new createjs.Shape();
    circle.graphics.beginFill("DeepSkyBlue").drawCircle(0, 0, 50);
    circle.x = 100;
    circle.y = 100;
    stage.addChild(circle);
    stage.update();
// All resources are available including downloaded images...

//    var licence_info_UPPA = ["L1", "L2", "L3"];
//    window.alert(licence_info_UPPA instanceof Array); // 'true' is displayed
//    window.alert(typeof licence_info_UPPA); // 'object' is displayed
//
//    window.console.log("windowIsLoaded");
//    window.alert(JSON.stringify(window.location.hostname)); // '"localhost"' may be displayed

    alert(Compte_bancaire_JS6.Information());
    try {
        var cb = new Compte_bancaire_JS6("cb", 100.);
        cb.appliquer_taux_interet(); // This fails since 'Compte_bancaire' is implemented as an abstract class
        window.alert(cb.cumul_interets());
    } catch (e) {
        window.alert(e);
    }
    alert(Compte_cheque_JS6.Information());
    var cc = new Compte_cheque_JS6("cc", 200., 0.02, 100.);
    cc.appliquer_taux_interet();
    window.alert(cc.cumul_interets());

    var Franck = {};
    Object.defineProperty(Franck, "surname", {value: "Barbier", enumerable: true, configurable: true, writable: true});
//    window.alert(Franck.surname); // 'Barbier' is displayed

    Object.defineProperty(Franck, "nickname", {value: "Bab", enumerable: true, configurable: false, writable: false});
    try {
        Franck.nickname = "Other nickname";
    } catch (error) {
        window.alert(error.message); // '"nickname" is read-only' is displayed
    }
    try {
        delete Franck.nickname;
    } catch (error) {
        window.alert(error.message); // 'property "nickname" is non-configurable and can't be deleted' is displayed
    }

    Franck.skill = "JavaScript"; // Dynamic extension
    delete Franck.skill; // Dynamic suppression
//    if ("skill" in Franck === false)
//        window.alert("'Franck.skill' no longer exists...");

    Franck.severe = true;
//    alert(JSON.stringify(Franck)); // '{"surname":"Barbier","severe":true}' is displayed

//    for (var attribute in Franck) { // 'enumerable: true' for 'attribute'
//        if (typeof Franck[attribute] === "string")
//            alert("V1 " + Franck[attribute].toLocaleUpperCase());
//        if (Franck[attribute] instanceof String) // Does not match because '"Barbier"' not constructed as is: 'new String("Barbier")'
//            alert("V2 " + Franck[attribute].toLocaleUpperCase());
//        if (Franck[attribute].constructor === String)
//            alert("V3 " + Franck[attribute].toLocaleUpperCase());
//    }
//    alert(Franck.surname); // No side effect!

//    alert(Math.E); // '2.718281828459045' is displayed
//    alert(Math.ceil(Math.E)); // '3' is displayed

    Franck.my_array = [null, 0, "ABC"]; // Arrrgggglll, arrays may contain anything!

    Franck.my_array_processing = function () {
        for (var i = 0; i < this.my_array.length; i++) {
            this.my_array[i] = this.severe; // Great, 'this' is set to the appropriate "current" object!
        }
    };
    Franck.my_array_processing();
    window.alert(JSON.stringify(Franck)); // '{"surname":"Barbier","nickname":"Bab","severe":true,"my_array":[true,true,true]}' is displayed

    Franck.my_array_other_processing = function () {
        this.my_array.forEach(function (element) {
            window.alert("typeof 'this' in 'Franck.my_array_other_processing' : " + typeof this); // 'undefined' is displayed
            element = this.severe; // Arrrgggglll, 'this' is NOT set to the appropriate "current" object: 'TypeError: this is undefined' in the console
        }/*, this*/); // You got some trouble just before? Uncomment!
    };
    Franck.my_array_other_processing();

    var f = function (element) {
        window.alert(Franck === this); // 'true' is displayed
        element = this.severe;
    };
    Franck.my_array_other_processing_2nde_version = function () {
        this.my_array.forEach(f.bind(this));
    };
    Franck.my_array_other_processing_2nde_version();


//alert(typeof true); // 'boolean'
//alert(typeof 2016); // 'number'
//alert(typeof '2016'); // 'string'
//    var x;
//    alert(typeof x); // 'undefined'
//    alert(typeof null); // 'object'

    var my_function = function (my_parameter) {
//        window.alert(this.surname); // 'Barbier' is displayed since 'Franck' is substituted for 'this' below
        return ++my_parameter;
    };
//    alert(my_function(0)); // '1' is displayed
//    alert(typeof my_function); // 'function' is displayed
//    alert(my_function.constructor === Function); // 'true' is displayed
//    window.alert("call: " + my_function.call(Franck, 0)); // First arg. plays the role of 'this' inside the function while '0' is substituted for 'my_parameter'
//    window.alert("apply: " + my_function.apply(Franck, [0])); // First arg. plays the role of 'this' inside the function while '[0]' is a one-element array containing '0' as unique element

//    var f = function (g) { // 'f' is defined
//        g();
//    };
//    f(function () {
//        window.alert("This no-name function plays the role of 'g' in 'f'");
//    }); // 'f' is called
    try {
        var cb = new Compte_bancaire("cb", 100.);
        cb.appliquer_taux_interet(); // This fails since 'Compte_bancaire' is implemented as an abstract class
        window.alert(cb.cumul_interets());
    } catch (e) {
        window.alert(e);
    }
    var cc = new Compte_cheque("cc", 200., 0.02, 100.);
    cc.appliquer_taux_interet();
    window.alert(cc.cumul_interets());
}

var Compte_bancaire = function (id, solde) {
    this._id = id;
    this._solde = solde;
    this._cumul_interets;
};
Compte_bancaire.prototype.id = function () {
    return this._id;
};
Compte_bancaire.prototype.solde = function () {
    return this._solde;
};
Compte_bancaire.prototype.cumul_interets = function () {
    return this._cumul_interets;
};
Compte_bancaire.prototype.mise_a_jour = function (montant) {
    this._solde += montant;
    return this._solde;
};
Compte_bancaire.prototype.taux_interet = function () {
    throw "Undefined function due to abstract nature of the class...";
};
Compte_bancaire.prototype.appliquer_taux_interet = function () {
    this._cumul_interets = this._solde * (1. + (this.taux_interet() / 100.));
};
Compte_bancaire.prototype.compareTo = function (cb) {
    return this._solde > cb._solde ? 1 : this._solde < cb._solde ? -1 : 0;
};
var Compte_cheque = function (id, solde, taux_interet, seuil) {
    Compte_bancaire.call(this, id, solde); // 'super' in Java
    this._taux_interet = taux_interet;
    this._seuil = seuil;
};
Compte_cheque.prototype = Object.create(Compte_bancaire.prototype); // Inheritance link
Compte_cheque.prototype.constructor = Compte_cheque;
Compte_cheque.prototype.taux_interet = function () {
    return this._taux_interet;
};
Compte_cheque.prototype.appliquer_taux_interet = function () {
    if (this._solde > this._seuil) {
        Compte_bancaire.prototype.appliquer_taux_interet.call(this); // 'super' in Java
    }
};
