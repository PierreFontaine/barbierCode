/*
 * jQuery_Deferred_Object.js
 */

"use strict";

function main() {
    var $event_1 = $.Deferred();
    var $event_2 = $.Deferred();
    var $event_3 = $.Deferred();

    var to_do = function (event_1_name, event_2_name, event_3_name) { // Event handling
        swal("State of " + event_1_name + ": " + $event_1.state() + '\n' +
                "State of " + event_2_name + ": " + $event_2.state() + '\n' +
                "State of " + (event_3_name === undefined ? "-no parameter set for event_3_name- " : event_3_name + ": ") + $event_3.state()); // 'sweetalert.js' nice library
    };
    // Removing comments leads to have no triggering for 'to_do' since 'event_3' is rejected (see below)...
    $.when($event_1, $event_2/*, $event_3*/).done(to_do); // Event handling policy

    (function () {
        window.setTimeout(sending_event_1, 3000); // One shot only
        function sending_event_1() {
            console.log("Waiting 3 sec. before sending " + "event_1");
            $event_1.resolve("event_1");
        }
    })();

    (function () {
        window.setTimeout(sending_event_2, 2000); // One shot only
        function sending_event_2() {
            console.log("Waiting 2 sec. before sending " + "event_2");
            $event_2.resolve("event_2");
        }
    })();

    (function () {
        window.setTimeout(rejecting_event_3, 1000); // One shot only
        function rejecting_event_3() {
            console.log("Waiting 1 sec. before rejecting " + "event_3");
            $event_3.reject("event_3");
        }
    })();
}
